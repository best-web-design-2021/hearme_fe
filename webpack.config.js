require('dotenv').config({ path: __dirname + '/.env' })
const path = require("path")
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const CopyPlugin = require("copy-webpack-plugin")
const Dotenv = require("dotenv-webpack")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const CompressionPlugin = require("compression-webpack-plugin")
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin
const fs = require("fs")
const directoryPath = path.resolve("public")
const handleDir = () => {
    return new Promise((resolve, reject) => {
        fs.readdir(directoryPath, (err, files) => {
            if (err) {
                reject("Unable to scan directory: " + err)
            }
            resolve(files)
        })
    })
}

module.exports = async (env, agrv) => {
    const isDev = agrv.mode === "development"
    const isAnalyze = env && env.analyze
    const dirs = await handleDir()
    const copyPluginPatterns = dirs
        .filter(dir => dir !== "index.html")
        .map(dir => {
            return {
                from: dir,
                to: "",
                context: path.resolve("public")
            }
        })

    const basePlugins = [
        // new webpack.HotModuleReplacementPlugin(),
        new Dotenv(),
        new HtmlWebpackPlugin(
            Object.assign({
                inject: true,
                title: 'Hot Module Replacement',
                template: "public/index.html",
                paths: {
                    publicURL: isDev ? '' : process.env.APP_BASENAME_PATH || ''
                },
            },
                isDev ? {
                    minify: {
                        removeComments: true,
                        collapseWhitespace: true,
                        removeRedundantAttributes: true,
                        useShortDoctype: true,
                        removeEmptyAttributes: true,
                        removeStyleLinkTypeAttributes: true,
                        keepClosingSlash: true,
                        minifyJS: true,
                        minifyCSS: true,
                        minifyURLs: true,
                    }
                } : undefined
            )

        ),
        new CopyPlugin({
            patterns: copyPluginPatterns
        }),
        new MiniCssExtractPlugin({
            filename: isDev ? "static/css/[contenthash].css" : "static/css/[contenthash].css",
            chunkFilename: "static/css/[contenthash].chunk.css",
        }),
        new webpack.ProgressPlugin(),
        // new webpack.DefinePlugin({
        //     "process.env": {
        //         APP_BASENAME_PATH: JSON.stringify(process.env.APP_BASENAME_PATH) || JSON.stringify('/'),
        //     }
        // }),
    ]
    if (isDev) {
        // only enable hot in development
        basePlugins.push(new webpack.HotModuleReplacementPlugin());
    }
    let prodPlugins = [
        ...basePlugins,
        new CleanWebpackPlugin(),
        new CompressionPlugin({
            test: /\.(css|js|html|svg)$/
        })
    ]
    if (isAnalyze) {
        prodPlugins = [...prodPlugins, new BundleAnalyzerPlugin()]
    }
    return {
        entry: "./src/index.tsx",
        output: {
            path: path.resolve("build"),
            publicPath: isDev ? `/` : `${process.env.APP_BASENAME_PATH || ''}/`,
            filename: isDev ? "static/js/[name].[contenthash].js" : "static/js/[contenthash].js",
            chunkFilename: isDev ? 'static/js/[contenthash].chunk.js'
                : 'static/js/[contenthash:8].chunk.js',
            environment: {
                arrowFunction: false,
                bigIntLiteral: false,
                const: false,
                destructuring: false,
                dynamicImport: false,
                forOf: false,
                module: false
            }
        },
        optimization: {
            splitChunks: {
                chunks: 'all',
            },
            runtimeChunk: {
                name: entrypoint => `runtime-${entrypoint.name}`,
            },
        },
        module: {
            rules: [
                {
                    test: /\.(ts|tsx)$/,
                    use: ["ts-loader"],
                    exclude: /node_modules/
                },
                {
                    test: /\.(s[ac]ss|css)$/,
                    use: [
                        {
                            loader: isDev ? 'style-loader' : MiniCssExtractPlugin.loader
                        },
                        {
                            loader: "css-loader",
                            options: { sourceMap: isDev ? true : false }
                        },
                        {
                            loader: "sass-loader",
                            options: { sourceMap: isDev ? true : false }
                        }
                    ],
                    exclude: /\.module\.(s[ac]ss|css)$/
                },
                {
                    test: /\.css$/,
                    use: [
                        "postcss-loader"
                    ],

                },
                {
                    test: /\.(s[ac]ss|css)$/,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                esModule: true,
                                modules: {
                                    namedExport: false,
                                },
                            },
                        },
                        {
                            loader: 'css-loader',
                            options: {
                                esModule: true,
                                modules: {
                                    namedExport: false,
                                    localIdentName: isDev ? '[contenthash:8]__[local]' : '__[contenthash:6]__[contenthash:10]__[contenthash:3]',
                                },
                            },
                        },
                        {
                            loader: "sass-loader",
                            options: { sourceMap: isDev ? true : false }
                        }
                    ],
                    include: /\.module\.(s[ac]ss|css)$/
                },
                {
                    test: /\.(eot|ttf|woff|woff2)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: isDev ? "[path][name].[ext]" : "static/fonts/[name].[ext]"
                            }
                        }
                    ]
                },
                {
                    test: /\.(png|svg|jpg|gif|jpeg)$/,
                    use: [
                        {
                            loader: "file-loader",
                            options: {
                                name: isDev
                                    ? "[path][name].[ext]"
                                    : "static/media/[name].[contenthash:6].[ext]"
                            }
                        }
                    ]
                }
            ]
        },
        resolve: {
            extensions: [".tsx", ".ts", ".jsx", ".js", '.css', 'json'],
            alias: {
                "@": path.resolve("src"),
                "@@": path.resolve(),
                "@assets": path.resolve("src/assets"),
                "@common": path.resolve("src/common"),
                "@components": path.resolve("src/components"),
                "@config": path.resolve("src/config"),
                "@constants": path.resolve("src/constants"),
                "@context": path.resolve("src/context"),
                "@graphql": path.resolve("src/graphql"),
                "@pages": path.resolve("src/pages"),
                "@routers": path.resolve("src/routers"),
                "@tools": path.resolve("src/tools"),
                "@utils": path.resolve("src/utils")
            }
        },
        // devtool: isDev ? "inline-source-map" : false,
        devtool: false,
        devServer: {
            contentBase: "public",
            port: process.env.PORT ? parseInt(process.env.PORT) : 3000,
            hot: true,
            watchContentBase: true,
            historyApiFallback: true,
            open: true,
            // transportMode: 'ws',
        },
        plugins: isDev ? basePlugins : prodPlugins,
        performance: {
            maxEntrypointSize: 800000 //  Khi có 1 file build vượt quá giới hạn này (tính bằng byte) thì sẽ bị warning trên terminal.
        }
    }
}