
- Demo: https://hearme.techmark.cf/

- Setup Git: https://git-scm.com/ (hoặc có thể tải xuống bằng file zip và giải nén ra)

- Setup nodejs: https://nodejs.org/en/

- Clone Project

`git clone git@gitlab.com:best-web-design-2021/hearme_fe.git`

- Vào thư mục project

`cd hearme_fe`

- Copy file môi trường .env

`cp .env.example .env`

- Cài đặt dependency

`npm i`

- Start pj

`npm start`