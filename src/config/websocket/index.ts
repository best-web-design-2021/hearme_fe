import { ACCESS_TOKEN } from "@/constants"
import { Socket } from "socket.io-client"

const io = require("../../config/websocket/socket.io.min")
const socket: Socket = io(process.env.WEBSOCKET_URI || "/", {
    autoConnect: false,
    auth: {
        token: 'abc'
    },
    query: "token=" + localStorage.getItem(ACCESS_TOKEN),
    extraHeaders: {
        Authorization: "Bearer authorization_token_here"
    }
})


export { socket }
