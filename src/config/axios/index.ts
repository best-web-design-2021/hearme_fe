import { ACCESS_TOKEN } from "@constants"
import axios from "axios"

const urn = process.env.GRAPHQL_URN || `/`

const axiosClient = axios.create({
  baseURL: urn || "http://localhost:4200",
  headers: {
    "access-token": localStorage.getItem(ACCESS_TOKEN)
  }
})

export { axiosClient }
