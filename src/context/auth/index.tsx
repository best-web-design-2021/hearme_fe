import { IAuthState } from "@common"

export const initAuthState: IAuthState = {
  isAuth: false,
  user: null
}

export * from "./reducer"
