import moment from "moment"
import { GAppointment } from "./interface"

export const appointmentToTime = (time: GAppointment): GAppointment => {
    const date = moment(time.date)
    return ({
        from: date.clone().startOf('day').add(time.from, 'hour').valueOf(),
        to: date.clone().startOf('day').add(time.to, 'hour').valueOf(),
        date: date.startOf('day').valueOf()
    })
}
