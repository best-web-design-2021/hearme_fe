import {
  DocumentNode,
  MutationHookOptions,
  OperationVariables,
  TypedDocumentNode,
  useMutation
} from "@apollo/client"

interface ServiceMutationHookOptions<TData, TVariables> {
  options?: MutationHookOptions<TData, TVariables>
}

/**
 * Custom hooks for mutate graphql with multiple endpoints
 * @param {DocumentNode | TypedDocumentNode} query - A GraphQL mutation document parsed into an AST by function gql from graphql-tag
 * @param {ServiceMutationHookOptions} [customOptions] - Include options to customize `useMutation` hook
 * @param {MutationHookOptions} [customOptions.options] - supported options for the `useMutation` hook
 * @param {GraphQLServicesList} [service="default"] - service name of graphql endpoint
 */
function useServiceMutation<TData = any, TVar = any>(
  query: DocumentNode | TypedDocumentNode<TData, OperationVariables & TVar>,
  customOptions?: ServiceMutationHookOptions<TData, OperationVariables & TVar>
) {
  const opts: MutationHookOptions<TData, TVar> = customOptions?.options || {}
  return useMutation<TData, OperationVariables & TVar>(query, opts)
}

export { useServiceMutation }
