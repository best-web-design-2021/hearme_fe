import {
    DocumentNode,
    LazyQueryHookOptions,
    OperationVariables,
    TypedDocumentNode,
    useLazyQuery
  } from '@apollo/client'
  
  interface ServiceLazyQueryHookOptions<TData, TVariables> {
    options?: LazyQueryHookOptions<TData, TVariables>
  }
  
  /**
   * Custom hooks for query graphql with multiple endpoints
   * @param {DocumentNode | TypedDocumentNode} query - A GraphQL query document parsed into an AST by function gql from graphql-tag
   * @param {ServiceLazyQueryHookOptions} [customOptions] - Include options to customize `useQuery` hook
   * @param {QueryHookOptions} [customOptions.options] - supported options for the `useQuery` hook
   * @param {GraphQLServicesList} [service="default"] - service name of graphql endpoint
   */
  function useServiceLazyQuery<TData = any, TVar = any>(
    query: DocumentNode | TypedDocumentNode<TData, TVar | OperationVariables>,
    customOptions?: ServiceLazyQueryHookOptions<TData, TVar | OperationVariables>
  ) {
    const opts = customOptions?.options || {}
    opts.context = { ...opts.context }
    return useLazyQuery<TData, TVar | OperationVariables>(query, opts)
  }
  
  export default useServiceLazyQuery
  