import {
  DocumentNode,
  OperationVariables,
  QueryHookOptions,
  TypedDocumentNode,
  useQuery
} from "@apollo/client"

interface ServiceMutationHookOptions<TData, TVariables> {
  options?: QueryHookOptions<TData, TVariables>
}

function useServiceQuery<TData = any, TVar = any>(
  query: DocumentNode | TypedDocumentNode<TData, TVar | OperationVariables>,
  customOptions?: ServiceMutationHookOptions<TData, TVar | OperationVariables>
) {
  const opts = customOptions?.options || {}
  return useQuery<TData>(query, opts)
}

export { useServiceQuery }
