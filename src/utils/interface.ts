export interface GDoctor {
  _id?: string
  code?: string
  slogan?: string
  note?: string
  exprience?: number
  offeres?: string[]
  specialties?: string[]
  cvLink?: string
  user?: GUser
  isDeleted?: boolean
  isActive?: boolean
  createdAt?: number
  createdBy?: GUser
  updatedAt?: number
  updatedBy?: GUser
}

export interface GUser {
  _id?: string
  username?: string
  firstName?: string
  lastName?: string
  fullName?: string
  email?: string
  // role?: GRoleOfUser;
  permissions?: string[]
  isLocked?: boolean
  isActive?: boolean
  createdAt?: number
  createdBy?: GUser
  updatedAt?: number
  updatedBy?: GUser
}

export enum EnumStatusSchedule {
  WAITING_DOCTOR_CONFIRM = "WAITING_DOCTOR_CONFIRM",
  WAITING_DOCTOR_CONFIRM_CANCEL = "WAITING_DOCTOR_CONFIRM_CANCEL",
  WAITING_CUSTOMER_CONFIRM = "WAITING_CUSTOMER_CONFIRM",
  ACCEPTED = "ACCEPTED"
}

export enum EnumChannelSchedule {
  ONLINE = "ONLINE",
  OFFLINE = "OFFLINE"
}

export enum ClientDoctor {
  CLIENT = "CLIENT",
  DOCTOR = "DOCTOR",
  OTHER = "OTHER"
}

export interface GSchedule {
  _id?: string;
  code?: string;
  doctor?: GDoctor;
  client?: GUser;
  appointment?: GAppointment;
  note?: string;
  channel?: EnumChannelSchedule;
  status?: EnumStatusSchedule;
  isMe?: ClientDoctor;
  isDeleted?: boolean;
  isActive?: boolean;
  createdBy?: GUser;
  createdAt?: number;
  updatedBy?: GUser;
  updatedAt?: number;
}
export interface GAppointment {
  from?: number;
  to?: number;
  date?: number;
}


export enum EnumEvent {
  NEW_SCHEDULES = "NEW_SCHEDULES",
  DENIED_SCHEDULE = "DENIED_SCHEDULE",
  ACCEPTED_SCHEDULE = "ACCEPTED_SCHEDULE"
}