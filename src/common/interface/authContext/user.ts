import { TAppPermission } from "../prermission"

export type TUser = {
  username: string
  fullName: string
  email?: string
  address?: string
  picture?: string
  permissions: TAppPermission[]
  role: string
  _id: string
}
