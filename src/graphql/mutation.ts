import { gql } from "@apollo/client"

export const LOGIN = gql`
  mutation login($info: LoginInput!) {
    login(info: $info) {
      token
      userId
    }
  }
`

export const LOGIN_GOOGLE = gql`
  mutation loginGoogle($token: String!) {
    loginGoogle(token: $token) {
      token
      userId
    }
  }
`


export const LOGOUT = gql`
  mutation {
    logout
  }
`
export const REGIS_DOCTOR = gql`
  mutation registerDoctor($input: RegisterDoctorInput!) {
    registerDoctor(input: $input) {
      _id
      code
    }
  }
`
export const BOOK_SCHEDULE = gql`
  mutation bookSchedule($input: BookScheduleInput!) {
    bookSchedule(input: $input) {
      _id
      code
      appointment {
        from
        to
        date
      }
      doctor {
        _id
        code
        user {
          _id
          username
          fullName
        }
      }
      channel
      status
    }
  }
`

export const CONFIRM_SCHEDULE = gql`
mutation confirmSchedule($id: ID!){
  confirmSchedule(id: $id){
    _id
    code
  }
}
`

export const CANCEL_SCHEDULE = gql`
mutation cancelSchedule($id: ID!){
  cancelSchedule(id: $id){
    _id
    code
  }
}
`

export const DENIED_SCHEDULE = gql`
mutation deniedSchedule($id: ID!){
  deniedSchedule(id: $id){
    _id
    code
  }
}
`

