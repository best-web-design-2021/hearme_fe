import { gql } from "@apollo/client"

export const ME = gql`
  query {
    me {
      _id
      username
      email
      firstName
      lastName
      fullName
      role {
        _id
        code
        description
      }
      picture
      permissions
      createdAt
      createdBy {
        _id
        username
      }
    }
  }
`

export const USERS = gql`
  query {
    users {
      _id
      username
      email
      firstName
      lastName
      fullName
      isLocked
      role {
        _id
        code
        description
      }
      permissions
      isActive
      createdAt
      createdBy {
        _id
        username
      }
    }
  }
`
export const DOCTOR_RS = gql`
  query {
    doctorsRS {
      doctor {
        _id
        code
        user {
          _id
          username
          firstName
          lastName
          fullName
        }
        slogan
        note
        exprience
        offeres
        specialties
      }
      freetimes {
        from
        to
        date
      }
    }
  }
`

export const RS_TIME_DOCTOR = gql`
  query rSScheduleOfDoctor($time: Float) {
    rSScheduleOfDoctor(time: $time)
  }
`

export const MY_DOCTOR = gql`
  query {
    myDoctor {
      _id
      code
      isActive
      slogan
      user {
        username
        fullName
        lastName
      }
      cvLink
      exprience
      offeres
      specialties
    }
  }
`

export const MY_SCHEDULES = gql`
  query {
    myBookSchedules {
      _id
      code
      appointment {
        from
        to
        date
      }
      doctor {
        _id
        code
        exprience
        user {
          _id
          username
          fullName
        }
      }
      isMe
      channel
      client {
        _id
        fullName
        email
      }
    }
  }
`

export const MY_SCHEDULES_APPROVE = gql`
  query {
    myBookSchedulesApprove {
      _id
      code
      appointment {
        from
        to
        date
      }
      doctor {
        _id
        code
        exprience
        user {
          _id
          username
          fullName
        }
      }
      isMe
      channel
      status
      client {
        _id
        fullName
        email
      }
    }
  }
`

export const MY_SCHEDULES_UPCOMING = gql`
  query {
    myBookSchedulesUpcoming {
      _id
      code
      appointment {
        from
        to
        date
      }
      doctor {
        _id
        code
        exprience
        user {
          _id
          username
          fullName
        }
      }
      isMe
      channel
      status
      client {
        _id
        fullName
        email
      }
    }
  }
`



export const GEN_TOKEN_MEETING = gql`
query genTokenMeeting($id: ID!){
  genTokenMeeting(id: $id){
    token
    userId
  }
}
`


export const RS_SCHEDULE_OF_DOCTOR = gql`
query rSScheduleOfDoctor($user_id: String, $time: Float){
  rSScheduleOfDoctor(user_id: $user_id, time: $time){
    from
    to
    date
  }
}
`
