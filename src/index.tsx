import React from "react"
import ReactDOM from "react-dom"
import { AppRouters } from "./pages"
import reportWebVitals from "./reportWebVitals"
import "bootstrap/dist/css/bootstrap.min.css"
import "antd/dist/antd.css"
import "./index.scss"
import "./index.css"

ReactDOM.render(<AppRouters />, document.getElementById("app-root"))

reportWebVitals()
