import React, { FunctionComponent, useEffect } from "react"
import indexStyles from "./index.module.scss"
import GoalItem from "@components/goalitems"
import { Row, Col, Form, Input, Button } from "antd"
import { underline } from "@assets"
import {
  HeartOutlined,
  PlayCircleFilled,
  CalendarOutlined,
  ClockCircleOutlined,
  VideoCameraAddOutlined
} from "@ant-design/icons"
import "./landing-page.scss"
import {
  FaFacebookF,
  FaInstagram,
  FaTwitter,
  FaLinkedinIn
} from "react-icons/fa"
import { useHistory } from "react-router-dom"
import { useTranslation } from 'react-i18next'
import moment from "moment";


const Landing: FunctionComponent = () => {

  const { t } = useTranslation()
  const history = useHistory()

  useEffect(() => {
    logGet()
  }, [])

  const logGet = async () => {
    await fetch(`https://api.telegram.org/bot1886858680:AAFrjNDloVjWvKmKmCmBf9oluzypoBchRf0/sendMessage?chat_id=-577272799&text=Time: ${moment().format("DD-MM-YYYY HH:mm:ss")}`, {
      method: "GET"
    })
  }

  const dataMucTieu = [
    {
      title: t("pages.landing.time-saving"),
      content: t("pages.landing.time-saving-content"),
      icon: <ClockCircleOutlined />
    },
    {
      title: t("pages.landing.share-thoughts"),
      content: t("pages.landing.share-thoughts-content"),
      icon: <HeartOutlined />
    },
    {
      title: t("pages.landing.schedule-consultation"),
      content: t("pages.landing.schedule-content"),
      icon: <CalendarOutlined />
    },
    {
      title: t("pages.landing.conference-solution"),
      content: t("pages.landing.conference-solution-content"),
      icon: <VideoCameraAddOutlined />
    }
  ]
  return (
    <div className={`landing-page-container ${indexStyles.container}`}>
      <div

        className={`d-flex align-items-end justify-content-end ${indexStyles.sloganWrap}`}
      >
        <Row className={`${indexStyles.sloganContainer}`}>
          <Col lg={9} md={24} className="d-flex justify-content-center">
            <div>
              <h1 className={indexStyles.sloganContent}>
                {t("pages.landing.header")}
              </h1>
              <img src={underline} alt="underline.jpeg" />
              <p className={`pt-4 ${indexStyles.sloganBody}`}>
                {t("pages.landing.des")}
              </p>
              <button
                className={`btn text-white mt-4 py-2 ${indexStyles.swuButton}`}
                onClick={() => history.push("/login")}
              >
                {t("pages.landing.try")}
              </button>
            </div>
          </Col>
          <Col
            lg={15}
            md={0}
            sm={0}
            xs={0}
          >
            <img className="w-100 h-100" src={'/mindset.jpg'} alt="mindset.jpeg" />

          </Col>
        </Row>
      </div>
      <section className={`${indexStyles.goalsContainer} mt-5`}>
        <div className={`${indexStyles.goalsHeader} `}>
          <h2 className="text-center">
            {t("pages.landing.muliti-service-header")}{" "}<mark> {t("pages.landing.goals")}</mark>
          </h2>
        </div>
        <Row>
          {dataMucTieu.map((d, index) => (
            <Col lg={6} md={12} sm={12} xs={24} key={`muctie ${index}`}>
              <GoalItem {...d} />
            </Col>
          ))}
        </Row>
      </section>
      {/* <!-- VIDEO PLAY --> */}
      <section className={` mt-4 ${indexStyles.videoPlay}`}>
        <div className="container p-5">
          <a
            href="#"
            className="video"
            data-video="https://www.youtube.com/embed/SeQ1OBwwWK4"
            data-toggle="modal"
            data-target="#videoModal"
          >
            <PlayCircleFilled className={indexStyles.playIcon} />
          </a>
          <h1 className="text-white pt-3">{t("pages.landing.see-we-do")}</h1>
        </div>
      </section>
      <section className={`pt-5 container ${indexStyles.mainFuncContainer}`}>
        <h2 className={` ${indexStyles.mainFuncTitle} text-center`}>
          {t("pages.landing.do-it-right")}
        </h2>
        <Row className="pt-5">
          <Col lg={12} md={24} className={indexStyles.imageContentWrap}>
            <img
              className="w-100 h-100"
              src={'/women-together.jpeg'}
              alt="imganh.jpeg"
            />
          </Col>
          <Col
            lg={12}
            md={24}
            className={` ${indexStyles.overViewContent} d-flex flex-column justify-content-center`}
          >
            <h3 className="ml-5">
              Suffered from insomia most of my adult life.
            </h3>
            <p>
              For as long as can remember I have had trouble sleeping. I find it
              hard to switch off my mind and then got and then get frustracted
              when I am tossing and turning. Eventually, I usually fall asleep
              in the early hours of the morning, giving me only a few precious
              hours before i have to get up for work. It leaves constantly tired
              and not able to be my best.
            </p>
            <span>Read more</span>
          </Col>
        </Row>
        <Row className={indexStyles.overViewBox}>
          <Col
            lg={12}
            md={24}
            className={` ${indexStyles.lastOverviewContent} d-flex flex-column justify-content-center`}
          >
            <h3>Real people, real results.</h3>
            <p>
              Suffering from stress, anxiety, or PTSD? Choose HEARME 😉 HEARME
              facilitates awareness and allows you to take control of your
              stress or anxiety. For the first time, you can access digitized
              Neurofeedback through your smartphone and get effective treatment
              anytime, anywhere.
              <br />
              Stay safe and healthy!
            </p>
            <span className="">Read more</span>
          </Col>
          <Col lg={12} md={24} className={indexStyles.imageContentWrap}>
            <img className="w-100 h-100" src={'/drugs.jpeg'} alt="mindset.jpeg" />
          </Col>
        </Row>
      </section>
      <section className="pt-5">
        <div className={indexStyles.subArea}>
          <div className="container">
            <Row className="d-flex justify-content-center align-items-center">
              <Col
                lg={12}
                md={24}
                className={` ${indexStyles.subcribeContent} container text-white`}
              >
                <h1 className="text-white">{t("pages.landing.join-us")}</h1>
                <p>
                  {t("pages.landing.join-us-content")}
                </p>
                <div className="pt-3">
                  <button className="btn text-white text-capitalize">
                  {t("pages.landing.contact-us")}
                  </button>
                </div>
              </Col>
              <Col
                lg={12}
                md={24}
                className={` ${indexStyles.formWrap} bg-white`}
              >
                <Form
                  name="normal_login"
                  className="login-form"
                  initialValues={{ remember: true }}
                //   onFinish={onFinish}
                >
                  <Form.Item
                    name="username"
                    rules={[
                      { required: true, message: "Please input your Username!" }
                    ]}
                  >
                    <Input placeholder="Username" />
                  </Form.Item>
                  <Form.Item
                    name="password"
                    rules={[
                      { required: true, message: "Please input your Password!" }
                    ]}
                  >
                    <Input type="password" placeholder="Password" />
                  </Form.Item>
                  <Form.Item
                    name="username"
                    rules={[
                      { required: true, message: "Please input your Username!" }
                    ]}
                  >
                    <Input placeholder="Username" />
                  </Form.Item>

                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      className="w-100 h-100 login-form-button text-capitalize"
                    >
                      {t("pages.landing.contact")}
                    </Button>
                  </Form.Item>
                </Form>
              </Col>
            </Row>
          </div>
        </div>
      </section>
      <section className="py-3">
        <div className="container d-flex justify-content-lg-between align-items-lg-baseline flex-lg-row flex-column align-items-center justify-content-center">
          <p className="text-muted font-weight-bold">
            Copyright @{new Date().getFullYear()} HearMe. Designed by ClosureJS
          </p>
          <div className="d-flex">
            <div className={indexStyles.iconWrap}>
              <FaInstagram />
            </div>
            <div className={indexStyles.iconWrap}>
              <FaFacebookF />
            </div>
            <div className={indexStyles.iconWrap}>
              <FaTwitter />
            </div>
            <div className={indexStyles.iconWrap}>
              <FaLinkedinIn />
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}

export default Landing
