import React, { lazy } from "react"
import { Switch, Route } from "react-router-dom"
import { routersUser } from "@routers"
import { UserPage } from "@/components/layout"
import NotFoundPage from "../404"

const Components = {}

routersUser.forEach(route => {
  Components[route.component] = lazy(() => import(`@pages/${route.component}`))
})
interface AppUserProps {}

const AppUser: React.FC<AppUserProps> = props => {
  return (
    <Switch>
      {routersUser.map((route, index) => (
        <Route
          key={index}
          exact={route.exact}
          path={route.path}
          render={routeProps => {
            const Component = Components[route.component]
            return (
              <UserPage {...props} {...route}>
                <Component {...props} {...routeProps} route={route} />
              </UserPage>
            )
          }}
        />
      ))}
      <Route render={() => <NotFoundPage />} />
    </Switch>
  )
}

export default AppUser
