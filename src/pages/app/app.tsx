import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect
} from "react-router-dom"
import React, { lazy, useEffect, useState } from "react"
import { routersNotAuth } from "@routers"
import AppAuth from "./appAuth"
import AppUser from "./appUser"
import { useAuth } from "@context"
import { appPermisions, ACCESS_TOKEN, LANGUAGE } from "@constants"
import { Loging } from "@components/loging"
// import { Snackbar } from '@material-ui/core'
import { useOnlineStatus } from "@utils"
import { LayoutNotAuth } from "@/components/layout"
import { ME } from "@/graphql/query"
import { queryData } from "@/tools/apollo/func"
import { checkPermission } from "@common"
import Landing from "../landing-page"
import NotFoundPage from "../404"
import { useTranslation } from 'react-i18next'
import { socket } from "@config"

const Components = {}

routersNotAuth.forEach(route => {
  Components[route.component] = lazy(() => import(`@pages/${route.component}`))
})
interface AppProps {}

const AppRouters: React.FC<AppProps> = props => {
  const { isAuth, user, dispatchAuth } = useAuth()
  const [loging, setLoging] = useState<boolean>(true)
  const internet = useOnlineStatus()
  const { i18n } = useTranslation()

  useEffect(() => {
    console.log(isAuth)
    if (!isAuth) {
      const sk = socket?.disconnect()
      console.log(sk)
    }
    else socket?.connect()
  }, [isAuth])

  useEffect(() => {
    verifyAuth()
  }, [dispatchAuth])

  const verifyAuth = async () => {
    const token = localStorage.getItem(ACCESS_TOKEN)
    if (!token) {
      setLoging(false)
      return
    }
    const me = await fetchMe()
    if (!me) {
      setLoging(false)
      return
    }
    dispatchAuth({
      type: "SET_AUTHEN",
      payload: {
        isAuth: true,
        user: me
      }
    })
    setLoging(false)

    return
  }

  const fetchMe = async () => {
    try {
      const { data, errors } = await queryData(ME)
      if (errors) return null
      return data.me
    } catch (error) {
      // error?.graphQLErrors
    }
  }

  // user

  return (
    <React.Fragment>
      {loging ? (
        <Loging />
      ) : (
        <Router basename={process.env.APP_NAME ? process.env.APP_NAME : "/"}>
          <Switch>
            <Route
              path="/admin"
              key="/admin page"
              render={routeProps => {
                if (
                  isAuth &&
                  user &&
                  checkPermission(
                    user?.permissions || [],
                    appPermisions.ADMIN_PAGE
                  )
                )
                  return <AppAuth {...props} {...routeProps} />
                localStorage.setItem("PREV_ROUTER", "/admin")
                return <Redirect to="/login?auth=true" />
              }}
            />
            {routersNotAuth.map((route, index) => (
              <Route
                key={index}
                exact={route.exact}
                path={route.path}
                render={routeProps => {
                  const Component = Components[route.component]
                  return (
                    <LayoutNotAuth {...route}>
                      <Component {...props} {...routeProps} route={route} />
                    </LayoutNotAuth>
                  )
                }}
              />
            ))}
            <Route
              path="/"
              key="/user page"
              render={routeProps => {
                if (isAuth && user)
                  return <AppUser {...props} {...routeProps} />
                return (
                  <LayoutNotAuth>
                    <Landing />
                  </LayoutNotAuth>
                )
              }}
            />
            <Route path="*" render={() => <NotFoundPage />} />
          </Switch>
        </Router>
      )}
      {/* <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left'
        }}
        open={!internet.online}
        autoHideDuration={2}
        message={
          <div>
            <DisconnectOutlined /> &nbsp; Mất kết nối internet.
          </div>
        }
      /> */}
    </React.Fragment>
  )
}

export default AppRouters
