import { StoreContextProvider } from "@context"
import React, { useEffect } from "react"
import { HeaderProvider } from "@/utils"
import AppRouters from "./app"
import { ApolloProvider } from "@apollo/client"
import { client, socket } from "@config"
import { I18nextProvider } from 'react-i18next'
import { i18n } from '../../translation'

interface AppProps {}

const App: React.FC<AppProps> = () => {
  useEffect(() => {
    socket.on("connect", function () {
      console.log("Connected")

      socket.emit("events", { test: "test" })
      socket.emit("identity", 0, response => console.log("Identity:", response))
    })
    socket.on("events", function (data) {
      console.log("event", data)
    })
  }, [])

  return (
    <I18nextProvider i18n={i18n}>
      <ApolloProvider client={client}>
      <StoreContextProvider>
        <HeaderProvider>
          <AppRouters />
        </HeaderProvider>
      </StoreContextProvider>
    </ApolloProvider>
    </I18nextProvider>
  )
}

export default App
