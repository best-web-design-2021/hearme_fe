import React from "react"
import { Layout } from "antd"
import { post1, post2, post3, post4 } from "@assets"
import { PostComment } from "@components/user"
import { Swiper, SwiperSlide } from "swiper/react"
import SwiperCore, { Navigation } from "swiper/core"
import { IoIosClose } from "react-icons/io"
import { useHistory } from "react-router-dom"
import { map } from "lodash-es"
import "swiper/swiper.min.css"
import "swiper/components/navigation/navigation.min.css"
import "./styles.scss"
import styles from "./styles.module.scss"

// install Swiper modules
SwiperCore.use([Navigation])

const { Content, Sider } = Layout

const ViewPost = () => {
  const history = useHistory()
  const post = [post1, post2, post3, post4]
  const renderImage = () => {
    return post.map(img => {
      return (
        <SwiperSlide className={`${styles.swiperSlide} `}>
          <img className="w-100 h-100" src={img} alt="anh.jpeg" />
        </SwiperSlide>
      )
    })
  }

  return (
    <div className="view-post-wrap">
      <Layout className={` ${styles.wrap}`}>
        <Sider
          style={{
            background: "white",
            overflow: "auto",
            position: "fixed",
            right: 0
          }}
        >
          <PostComment />
        </Sider>
        <Layout className={styles.layoutContent}>
          <Content>
            <IoIosClose
              onClick={() => history.goBack()}
              className={`${styles.closeIcon} position-absolute text-white hover-pointer`}
              style={{}}
            />
            <Swiper navigation={true} className="mySwiper">
              {renderImage()}
            </Swiper>
            <div className="d-md-none">
              <PostComment />
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  )
}
export default ViewPost
