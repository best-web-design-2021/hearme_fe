import React, { useCallback, useState } from "react"
import { Layout } from "antd"
import "./styles.scss"
import styles from "./styles.module.scss"
import { CalendarSider, Schedule } from "@components/user"
import { useServiceQuery } from "@/utils/hooks/graphql"
import { GSchedule } from "@/utils"
import { MY_SCHEDULES } from "@/graphql"

const { Content, Sider } = Layout
interface ScheduleProps {}

const SchedulePage: React.FC<ScheduleProps> = () => {

  const [schedules, setSchedules] = useState<GSchedule[]>([]);

  const onSchedulesChange = useCallback((schedules: GSchedule[]) => {
    setSchedules(schedules)
  }, [])

  return (
    <Layout className={`${styles.wrap} `}>
      <Layout>
        <Sider
          style={{
            overflow: "auto",
            height: "100vh",
            position: "fixed",
            left: 0,
            top: 0
          }}
          width={385}
        >
          <div style={{ paddingTop: "80px" }}>
            <CalendarSider onSchedulesChange={onSchedulesChange} />
          </div>
        </Sider>
      </Layout>
      <Content style={{ marginLeft: "385px" }} className='d-md-block d-none'>
        <div className="p-3">
          <Schedule schedules={schedules} />
        </div>
      </Content>
    </Layout>
  )
}

export default SchedulePage
