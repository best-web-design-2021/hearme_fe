import React from "react"
import { PsychologistListItem, PsychologistFilter } from "@components/user"
import { Layout } from "antd"
import "./styles.scss"
import styles from "./styles.module.scss"

const { Content, Sider } = Layout

interface PsychologistListProps {}

const PsychologistListPage: React.FC<PsychologistListProps> = () => {
  return (
    <Layout className={`${styles.wrap} psychologist-list-page-wrap`}>
      <Sider
        className={`pt-4 pb-2 px-2 ${styles.siderLayout} d-lg-block d-xl-block d-xxl-block d-none`}
        width={260}
      >
        <div className="px-2 bg-white border-radius-12 shadow-css">
          <PsychologistFilter />
        </div>
      </Sider>
      <Layout className={styles.content}>
        <Content className="px-2 pt-4">
          <PsychologistListItem />
        </Content>
      </Layout>
    </Layout>
  )
}

export default PsychologistListPage
