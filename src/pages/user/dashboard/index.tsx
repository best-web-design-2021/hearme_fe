import React, { useEffect, useState } from "react"
import { Row, Col, Space, Avatar } from 'antd'
import styles from './styles.module.scss'
import { USERS } from "@graphql"
import { mutateData, queryData } from "@/tools/apollo/func"
import { Income, PatientActivity, Reports, LastestPatientList } from '@components/therapists'
import { CurrentMed, Panel, NextAppointment, CurrentConditions, AppointmentList, CurrentBill } from '@components/user/dashboard'
import { useAuth } from "@context"


interface DashboardPageProps { }


const DashboardPage: React.FC<DashboardPageProps> = () => {
  const { user } = useAuth()
  console.log('user', user.permissions[0])
  return (
    <div className={`pt-4 ${styles.wrap}`}>
      <Row>
        {
          user.permissions[0] == 'APP_DOCTOR_USER' ?
            <>
              <Col xl={10} lg={12} md={12} sm={13} xs={24} className='px-2'>
                <Income />
              </Col>
              <Col xl={14} lg={12} md={12} sm={11} xs={24} className='px-2 pt-2 pt-sm-0'>
                <Panel />
              </Col>
              <Col xl={14} lg={13} md={14} sm={24} className='pt-3 px-2'>
                <PatientActivity />
              </Col>
              <Col xl={10} lg={11} md={10} sm={24} className='pt-3 px-2'>
                <Reports />
              </Col>
              <Col className='pt-3 px-2' span={24}>
              <LastestPatientList />
            </Col>
            </> : <> <Col xl={8} lg={12} md={12} sm={12} xs={24} className='px-2'>
              <NextAppointment />
            </Col>
              <Col xl={8} lg={12} md={12} sm={12} xs={24} className='px-2 pt-2 pt-sm-0'>
                <CurrentBill />
              </Col>
              <Col xl={8} lg={12} md={12} sm={12} xs={24} className='pt-3 pt-xl-0 px-2'>
                <CurrentMed />
              </Col>
              <Col xl={10} lg={12} md={12} sm={12} className='pt-3 px-2'>
                <CurrentConditions />
              </Col>
              <Col xl={14} lg={24} md={24} sm={24} xs={24} className='pt-3 px-2'>
                <AppointmentList />
              </Col>

            </>
        }
      </Row>
     <div>
     </div>
    </div>
  )
}

export default DashboardPage
