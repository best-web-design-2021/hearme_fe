import React from "react"
import {
  ProfileBanner,
  Post,
  ProfileSider,
  ProfileRightSider
} from "@components/user"
import AddPost from "@components/add-post"
import { Layout } from "antd"
import styles from "./styles.module.scss"
import { useParams } from "react-router-dom"
import { useAuth } from "@context"

const { Content, Sider } = Layout

interface UserProfileProps {}

const UserProfilePage: React.FC<UserProfileProps> = () => {
  const { id } = useParams<{ id: string }>()
  const { user } = useAuth()

  return (
    <>
      <ProfileBanner />
      <Layout className={styles.wrap}>
        <Sider className={`p-3 ${styles.leftSider}`} width={350}>
          <ProfileSider />
        </Sider>
        <Layout>
          <Layout className={styles.layoutContent}>
            <Content className="px-3 pt-2">
              <div className={styles.profileSider}>
                <ProfileSider />
              </div>
              <ProfileRightSider
                isMyProfile={
                  id === user?.username || id === user?._id ? true : false
                }
                className="d-lg-none d-md-none"
              />
              {id === user?.username || id === user?._id ? <AddPost /> : null}

              <Post />
            </Content>
          </Layout>
          <Sider
            width={350}
            className={` px-2 ${styles.content} ${styles.rightSider} ${styles.layoutContent}`}
          >
            <ProfileRightSider
              isMyProfile={
                id === user?.username || id === user?._id ? true : false
              }
            />
          </Sider>
        </Layout>
      </Layout>
    </>
  )
}

export default UserProfilePage
