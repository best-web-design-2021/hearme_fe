import React, { useEffect, useState } from "react"
import { MHDay } from "@assets"
import { Steps, Divider } from "antd"
import styles from "./styles.module.scss"
import './styles.scss'
import { LoadingLazyComponent } from "@components/loading-page"
import { UpdateCVComponent, AcceptedRegister, PedingRegister } from '@components/therapists'
import { useServiceQuery } from "@/utils/hooks/graphql"
import { MY_DOCTOR } from "@/graphql"
import { GDoctor } from "@utils"
import './styles.scss'

const { Step } = Steps

interface TherapistRegisterProps {}

const TherapistRegister: React.FC<TherapistRegisterProps> = () => {
  const { data, refetch, loading } =
    useServiceQuery<{ myDoctor: GDoctor }>(MY_DOCTOR)
  const [step, setStep] = useState<number>(null)

  useEffect(() => {
    if (!data) return
    if (!data?.myDoctor) {
      setStep(0)
      return
    }
    if (!data?.myDoctor?.isActive) {
      setStep(1)
      return
    }
    if (data?.myDoctor?.isActive) {
      setStep(2)
      return
    }
  }, [data])

  return (
    <div className={`${styles.container} pt-4 pb-3 px-2 `}>
      <section
        className={`bg-white border-radius-12 p-3 h-100 shadow-css ${styles.contentWrap}`}
      >
        <Steps
          type="navigation"
          size="small"
          current={step}
          className={`w-100 ${styles.stepWrap}`}
        >
          <Step
            title="Step 1"
            status={step === 0 ? "process" : step > 0 ? "finish" : "wait"}
            description="Submit your CV"
            className={styles.step}
          />
          <Step
            title="Step 2"
            status={step === 1 ? "process" : step > 1 ? "finish" : "wait"}
            description="Awaiting Acceptance."
            className={styles.step}
          />
          <Step
            title="Step 3"
            status={step === 2 ? "finish" : "wait"}
            description="Check Information."
            className={styles.step}
          />
        </Steps>
        <div className='pt-4'>
          {
            loading ? <LoadingLazyComponent /> : (
              <>
                {
                  step === 0 && <UpdateCVComponent onFinish={refetch} />
                }
                {
                  step === 1 && <PedingRegister />
                }
                {
                  step === 2 && <AcceptedRegister doctorInformation={data?.myDoctor} />
                }
              </>
            )
          }

        </div>
      </section>
    </div>
  )
}

export default TherapistRegister
