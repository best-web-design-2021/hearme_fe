import React from "react"
import { Link } from "react-router-dom"
import { scuba } from "@assets"
import styles from "./styles.module.scss"
import ButtonComponent from "@components/button"
import { ArrowLeftOutlined } from "@ant-design/icons"

interface NotFoundPageProps {}

const NotFoundPage: React.FC<NotFoundPageProps> = () => {
  return (
    <div style={{ height: "100vh" }} className="d-flex justify-content-center">
      <div
        className={`${styles.container} d-flex flex-column justify-content-center align-items-center `}
      >
        <img className="empty-state-img" src={scuba} />
        <h1 className="font-weight-bolder pt-3 text-uppercase">error 404</h1>
        <p className=" pt-4 text-uppercase text-center px-2">
          even in the deep ocean, we can't seem to find this page
        </p>
        <p className={`text-center ${styles.text}`}>
          It's seem the page does not exist or has been removed
        </p>
        <Link to="/">
          <ButtonComponent
            className={`d-flex align-items-center justify-content-center text-white ${styles.button}`}
          >
            <ArrowLeftOutlined />
            <p className="m-0 text-white">GO BACK HOME</p>
          </ButtonComponent>
        </Link>
      </div>
    </div>
  )
}

export default NotFoundPage
