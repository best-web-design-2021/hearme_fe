import React from "react"
import styles from "./styles.module.scss"

interface HomePageProps {}

const HomePage: React.FC<HomePageProps> = () => {
  return (
    <div className={styles.container}>
      <img
        src="http://localhost:4200/file/download?id=8182dde0-b70c-11eb-b259-27a34794d0b7.jpg"
        style={{
          height: "100%"
        }}
        alt="mindset.jpeg"
      />
    </div>
  )
}

export default HomePage
