import React, { useEffect } from "react"
import { useHistory } from "react-router-dom"
import { useAuth } from "@context"
import { PREV_ROUTER } from "@constants"

import styles from "./styles.module.scss"
import "./index.css"
import { LoginForm } from "@/components"

interface LoginPageProps {}

const LoginPage: React.FC<LoginPageProps> = () => {
  const { isAuth } = useAuth()
  const history = useHistory()

  useEffect(() => {
    return () => localStorage.removeItem(PREV_ROUTER)
  }, [])

  useEffect(() => {
    if (isAuth) history.replace("/")
  }, [isAuth, history])

  return (
    <div
      className={`d-flex align-items-center  justify-content-end ${styles.container}`}
    >
      <section className={`${styles.formWrap} bg-white h-100 overflow-y-auto`}>
        <LoginForm />
      </section>
    </div>
  )
}

export default LoginPage
