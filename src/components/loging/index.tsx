import React from "react"
import styles from "./styles.module.scss"

interface LogingProps {}

const Loging: React.FC<LogingProps> = () => {
  return (
    <div className={styles.container}>
      <div className={styles.loader}></div>
    </div>
  )
}

export { Loging }
