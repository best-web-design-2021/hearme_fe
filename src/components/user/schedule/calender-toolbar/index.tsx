import React, { FunctionComponent, memo, useState } from "react"
import { LeftOutlined, RightOutlined } from "@ant-design/icons"
import styles from "../styles.module.scss"

interface CalendarToolbarProps {
  onView: any
  label: string
  views: any[]
  date: Date
  onNavigate: Function
}

const CalendarToolbar: FunctionComponent<CalendarToolbarProps> = memo(
  ({ onView, label, views, date, onNavigate }) => {
    const [viewName, setViewName] = useState("week")

    const goToBack = () => {
      let mDate = date
      let newDate = new Date(mDate.getFullYear(), mDate.getMonth() - 1, 1)
      onNavigate("prev", newDate)
    }

    const today = () => {
      onNavigate("today", new Date())
    }

    const goToNext = () => {
      let mDate = date
      let newDate = new Date(mDate.getFullYear(), mDate.getMonth() + 1, 1)
      onNavigate("next", newDate)
    }

    const setView = (view: string, onView: any) => {
      setViewName(view)
      onView(view)("h", viewName)
    }
    return (
      <div
        className={`${styles.toolbarWrap} d-lg-flex align-items-center justify-content-between`}
      >
        <div>
          <p className={`${styles.label} m-0`}>{label}</p>
          <div className={`${styles.calendar} d-flex align-items-center`}>
            <h3>8 patients today</h3>
          </div>
        </div>
        <div
          className={`d-flex align-items-center ${styles.viewWrap} bg-white shadow-css px-1 py-1 hover-pointer`}
        >
          {views.map((view, index) => {
            return (
              <div
                onClick={() => setView(view, onView)}
                className={`${
                  viewName === view ? styles.onView : ""
                } h-100 d-flex justify-content-center align-items-center text-capitalize ${
                  styles.test
                }`}
                key={`view ${index}`}
              >
                <p className={`${styles.viewText} m-0 text-center`}>{view}</p>
              </div>
            )
          })}
        </div>
        <div className={`d-flex align-items-center ${styles.navigationWrap}`}>
          <div
            className={`bg-white ${styles.iconWrap} hover-pointer p-1 d-flex align-items-center justify-content-center shadow-css`}
          >
            <div className="h-100 d-flex align-items-center justify-content-center" onClick={goToBack} >
              <LeftOutlined className={`  px-2  ${styles.icon}`} />
            </div>
          </div>
          <div
            className={`${styles.todayWrap} hover-pointer bg-white p-1 shadow-css`}
          >
            <div
              onClick={today}
              className={` h-100 d-flex justify-content-center align-items-center text-capitalize ${styles.test}`}
            >
              <p className={`${styles.viewText} m-0 text-center`}>Today</p>
            </div>
          </div>
          <div
            className={`bg-white hover-pointer shadow-css ${styles.iconWrap} p-1 d-flex align-items-center justify-content-center shadow-css`}
          >
            <div className="h-100 d-flex justify-content-center align-items-center" onClick={goToNext}>
              <RightOutlined className={` px-2  ${styles.icon}`} />
            </div>
          </div>
        </div>
      </div>
    )
  }
)

export default CalendarToolbar
