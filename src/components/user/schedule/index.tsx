import React, { useState, memo } from "react"
import { Calendar, momentLocalizer } from "react-big-calendar"
import moment from "moment"
import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop"
import { ClientDoctor, EnumChannelSchedule, GSchedule } from "@/utils"
import CalendarToolbar from "./calender-toolbar"
import "react-big-calendar/lib/addons/dragAndDrop/styles.scss"
import "react-big-calendar/lib/css/react-big-calendar.css"
import "./styles.scss"
import styles from "./styles.module.scss"
import { appointmentToTime } from "@utils"

const Localizer = momentLocalizer(moment)
const DnDCalendar = withDragAndDrop(Calendar)

interface ScheduleProps {
  schedules: GSchedule[]
}

const renderWeekHeader = ({ date, localizer }) => {
  return (
    <div className={`${styles.weekHeader}`}>
      <p className="m-0">{localizer.format(date, "DD")}</p>
      <span className="m-0 text-uppercase">
        {localizer.format(date, "dddd")}
      </span>
    </div>
  )
}

const setBGColor = type => {
  switch (type) {
    case "online":
      return styles.eventVideoCall
    case "offline":
      return styles.eventChat
    default:
      return styles.eventInperson
  }
}

const renderEvent = (event: { event: GSchedule }) => {
  const time = appointmentToTime(event.event.appointment)

  return (
    <div
      className={`p-2 py-3 ${styles.eventWrap} ${setBGColor(
       "online"
      )} d-flex justify-content-between flex-column h-100`}
    >
      <p className={styles.title}>{event?.event.isMe === ClientDoctor.CLIENT ? event?.event.doctor?.user?.fullName : event?.event.client?.fullName}</p>
      <p className={styles.time}>
        {Localizer.format(time.from, "hh:mm")} -{" "} {Localizer.format(time.to, "hh:mm")}
      </p>
    </div>
  )
}

const renderMonthHeader = ({ date, localizer }) => {
  return (
    <span className={`m-0 text-uppercase ${styles.monthHeader}`}>
      {localizer.format(date, "dddd")}
    </span>
  )
}

const Schedule: React.FC<ScheduleProps> = memo(({ schedules }) => {

  const events = schedules.map(d => {

    const time = appointmentToTime(d.appointment)
    return {
      ...d,
      title: d.isMe === ClientDoctor.CLIENT ? d.doctor.user.fullName : d.client.fullName,
      id: d._id,
      type: d.channel === EnumChannelSchedule.ONLINE ? "online" : 'offline',
      allDay: false,
      start: new Date(time.from),
      end: new Date(time.to)
    }
  })

  return (

    <div className={`p-3 ${styles.wrap}`}>
      <DnDCalendar
        format={"DD-MM-YYYY HH:mm"}
        step={60}
        timeslots={1}
        components={{

          week: {
            header: renderWeekHeader
          },
          month: {
            header: renderMonthHeader
          },
          event: renderEvent,
          toolbar: CalendarToolbar
        }}
        localizer={Localizer}
        defaultDate={new Date()}
        defaultView="month"
        events={events}
      />
    </div>
  )
})

export { Schedule }
