import React, { memo, useState } from "react"
import styles from './styles.module.scss'
import { Space, Avatar, Row, Col } from 'antd'
import { useAuth } from "@context"

interface UserInfoProps { }

const UserInfo: React.FC<UserInfoProps> = memo(() => {
    const { user } = useAuth()
    return (
        <>
            <div className={`bg-white border-radius-12 p-3 shadow-css ${styles.wrap}`}>
                <Space>
                    <Avatar size={50} shape='square' className='border-radius-12'> {user?.fullName?.charAt(0)}</Avatar>
                    <div className={styles.nameWrap}>
                        <h5 className='m-0'>{user?.fullName}</h5>
                        <p className='m-0'>21 years old</p>
                    </div>
                </Space>
                <Row className={`pt-3 ${styles.infoWrap}`}>
                    <Col span={8}>
                        <p className='m-0'>Height</p>
                        <span>185</span>
                    </Col>
                    <Col span={8}>
                        <p className='m-0'>Weight</p>
                        <span>83</span>
                    </Col>
                    <Col span={8}>
                        <p className='m-0'>Blood type</p>
                        <span>AB+</span>
                    </Col>
                </Row>
            </div>
        </>
    )
})

export { UserInfo }
