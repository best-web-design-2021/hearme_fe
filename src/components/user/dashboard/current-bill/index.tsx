import React, { memo } from "react"
import styles from './styles.module.scss'
import { Space, Tag, Divider, Avatar } from 'antd'
import moment from "moment"
import { CheckCircleOutlined } from '@ant-design/icons'


interface CurrentBillProps { }

const CurrentBill: React.FC<CurrentBillProps> = memo(() => {
    return (
        <>
            <section className={`bg-white ${styles.wrap} shadow-css p-3 border-radius-12`}>
                <h4 className='text-capitalize'>Current bills</h4>
                <div className='d-flex justify-content-between'>
                    <Space direction='vertical'>
                        <div>
                        <p className={`m-0 ${styles.label}`}>Your payments</p>
                        <h5 className={`pt-1 ${styles.amount}`}>$110.00</h5>
                        </div>
                        <div className={styles.statusCheck}>
                            <p className='m-0'>Paid on the <span>{moment().format("DD MMMM")}</span> to <span>Doctor name</span>
                            </p>
                        </div>
                    </Space>
                    <Space direction='vertical'>
                        <div>
                        <p className={`m-0 ${styles.label}`}>Status</p>
                        <Tag color="#28a745" className='mt-1'>success</Tag>
                        </div>
                        <div>  
                        </div>
                    </Space>
                </div>
                <Divider dashed/>
                <Space className={styles.therapistsInfo}>
                    <Avatar size={40} shape='square' className='border-radius-12'>Q</Avatar>
                    <Space direction='vertical'>
                        <p className='m-0'>Dr. Quynh Nga</p>
                    </Space>
                </Space>
            </section>
        </>
    )
})

export { CurrentBill }
