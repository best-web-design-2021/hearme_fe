import React, { memo } from "react"
import styles from './styles.module.scss'
import { Table, Space } from 'antd'
import moment from "moment"
import './styles.scss'
import { EyeOutlined, DeleteOutlined } from "@ant-design/icons"




interface AppointmentListProps { }

const AppointmentList: React.FC<AppointmentListProps> = memo(() => {
const test = () => {

}
    const columns = [
        {
            title: 'No',
            dataIndex: 'index',
            key: 'index',
            render: text => <a>{text}</a>,
            width: 60
        },
        {
            title: 'Date In',
            dataIndex: 'date',
            key: 'date',
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            render: text => <p className={`m-0 ${styles.name}`}>{text}</p>
        },
        {
            title: 'Therapies Offered',
            dataIndex: 'therapies',
            key: 'therapies',
            render: therapies => (
                <>
                  {therapies.map(item => {
                    return (
                        <div
                        className={`mx-1 mt-1 d-inline-block ${styles.tagItem} shadow-css py-2 px-3`}
                      >
                       <span>  {item}</span>
                      </div>
                    )
                  })}
                </>
              ),
        },
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            render: (text, record) => {
                return (
                    <Space size='large'>
                        <EyeOutlined className={styles.editIcon}/>
                        <DeleteOutlined className={`text-danger ${styles.deleteIcon}`}/>
                    </Space>
                )
            }
        },
    ]
    const data = [
        {
            index: '1',
            name: 'John Brown',
            age: 32,
            date: moment().format("DD/MM/YY"),
            gender: 'male',
            therapies: ['nice', 'developer'],
        },
        {
            index: '2',
            name: 'John Brown',
            date: moment().format("DD/MM/YY"),
            age: 32,
            gender: 'male',
            therapies: ['nice', 'developer'],
        },
        {
            index: '3',
            name: 'John Brown',
            age: 32,
            date: moment().format("DD/MM/YY"),
            gender: 'male',
            therapies: ['nice', 'developer'],
        },
        {
            index: '4',
            name: 'John Brown',
            date: moment().format("DD/MM/YY"),
            age: 32,
            gender: 'male',
            therapies: ['nice', 'developer'],
        },

        {
            index: '5',
            name: 'Jim Green',
            date: moment().format("DD/MM/YY"),
            age: 42,
            gender: 'female',
            therapies: ['loser'],
        },
    ]

    return (
        <section className={`${styles.wrap} appointment-list-wrap w-100 bg-white border-radius-12 p-3 shadow-css`}>
            <h4>Latest appointment data</h4>
            <option onChange={test}></option>
            <Table
            scroll={{ x: 600 }}
            bordered={false}
                className='mt-3 w-100'
                pagination={false}
                columns={columns} dataSource={data} />
        </section>
    )
})

export { AppointmentList }
