import React, { memo, useState } from "react"
import styles from './styles.module.scss'
import { useAuth } from "@context"
import { DailyMed } from '@components/user'
import ShowMore from '@components/show-more'
import { CgPill } from "react-icons/cg";


interface CurrentMedProps { }

const CurrentMed: React.FC<CurrentMedProps> = memo(() => {
    const { user } = useAuth()
    return (
        <>
            <section className={`bg-white ${styles.wrap} shadow-css p-3 border-radius-12`}>
                <div className={styles.container}>
              <div className='d-flex align-items-center justify-content-between'>
               <h4>Current Medications(3)</h4>
               <ShowMore menu=''/>
               </div>
              <div className={styles.content}>
               <DailyMed />
               </div>
               </div>
            </section>
        </>
    )
})

export { CurrentMed }
