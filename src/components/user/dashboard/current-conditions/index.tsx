import React, { memo, useState } from "react"
import styles from './styles.module.scss'
import ShowMore from '@components/show-more'
import { Space, Avatar, Button } from 'antd'
import { CalendarOutlined } from '@ant-design/icons'
import moment from "moment"

interface CurrentConditionsProps { }

const CurrentConditions: React.FC<CurrentConditionsProps> = memo(() => {

    return (
        <>
            <section className={`bg-white ${styles.wrap} shadow-css p-3 border-radius-12`}>
                <div className={styles.container}>
                <div className='d-flex align-items-center justify-content-between'>
                    <h4>Current Conditions</h4>
                    <ShowMore menu='' />
                </div>
                <p className={` pt-3 ${styles.content}`}>
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vel quisquam maxime eveniet quidem tenetur beatae deserunt officia asperiores deleniti labore tempora libero corrupti sit ea voluptate porro nemo, necessitatibus nulla. Cum ut error voluptates perspiciatis ipsam? Assumenda optio corporis natus quasi doloremque, quod vero fugiat repudiandae placeat sint sed facere.
                </p>
                <div className={`d-flex align-items-start justify-content-between ${styles.appointmentInfo}`}>
                   <Space direction='vertical'>
                    <Space direction='vertical' style={{height: '70px'}}>
                        <label className={styles.label}>Primary</label>
                        <Space>
                            <Avatar size={40} shape='square' className='border-radius-12'>Q</Avatar>
                            <p className='m-0'>Dr Mai Văn Quang</p>
                        </Space>
                    </Space>
                    <div >
                        <label className={styles.label}>Treatments</label>
                        <p className={`m-0 ${styles.pill}`}>Nasonex Aerosol</p>
                        <span className={styles.note}>1pills a day</span>
                    </div>
                    </Space>
                    <Space direction='vertical'>
                    <div style={{height: '70px'}}>
                        <label className={styles.label}>Date</label>
                        <p className='m-0 pt-3'>{moment().format("DD MMM, YYYY")}</p>
                    </div>
                    <Space direction='vertical'>
                        <label className={styles.label}>Next Appointment</label>
                        <Button type="dashed">
                            Schedule
                        </Button>
                    </Space>
                    </Space>
                </div>
                </div>
            </section>
        </>
    )
})

export { CurrentConditions }
