import React, { memo, useState } from "react"
import styles from './styles.module.scss'
import { useAuth } from "@context"
import ButtonComponent from '@components/button'
import { illustrationAnnoucement } from '@assets'

interface PanelProps { }

const Panel: React.FC<PanelProps> = memo(() => {
    const { user } = useAuth()
    return (
        <>
            <div className={`bg-white ${styles.wrap} shadow-css p-3 border-radius-12`}>
                <div className={`${styles.contentWrap}`}>
                    <h3>Hello, Anne</h3>
                    <div>
                    <div className={styles.textWrap}>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corrupti illum distinctio cum natus similique quos magni sunt iusto aliquid excepturi!
                    </p>
                    </div>
                    </div>
                    <p>Have a nice day!</p>
                    <ButtonComponent className={styles.btn}><span>Complete Report</span></ButtonComponent>
                </div>
                <div className={`${styles.imgWrap} w-50 p-3 d-xl-block d-none`}>
                    <img src={illustrationAnnoucement} className='w-100 h-100' />
                </div>
            </div>
        </>
    )
})

export { Panel }
