import React, { memo } from "react"
import styles from './styles.module.scss'
import { Avatar, Space, Divider } from 'antd'
import moment from "moment"
import './styles.scss'
import { PhoneFilled, VideoCameraFilled, CloseOutlined, CalendarOutlined } from "@ant-design/icons"

interface NextAppointmentProps { }

const NextAppointment: React.FC<NextAppointmentProps> = memo(() => {
    return (
        <>
            <section className={`bg-white next-appointment-wrap ${styles.wrap} shadow-css p-3 border-radius-12`}>
                <h4 className='text-capitalize'>Next appointment</h4>
                <Space size='middle' className='pt-2'>
                    <Avatar size={40} shape='square' className='border-radius-12'>T</Avatar>
                    <div>
                        <p className={`m-0 ${styles.name}`}>Dr.Mai Văn Quang</p>
                        <span className={styles.location}> 310 Nguyễn Văn Cừ</span>
                    </div>
                </Space>
                <div className={`pt-3 d-flex align-items-center justify-content-between ${styles.dateTimeWrap}`}>
                    <div>
                        <label className={styles.label}>Date</label>
                        <p>{moment().format('DD MMM YYYY')}</p>
                    </div>
                    <div>
                        <label className={styles.label}>Time</label>
                        <p>{moment().format('HH:MM A')}</p>
                    </div>
                </div>
                <div className='d-flex align-items-center justify-content-between'>
                    <div>
                        <div className={`py-2 px-3 text-capitalize ${styles.statusWrap}`}>Awaiting Acceptance</div>
                    </div>
                    <Space>
                    <div className={`rounded-circle hover-pointer d-flex align-items-center justify-content-center ${styles.iconWrap}`}><PhoneFilled className='text-warning'/> </div>
                       <div className={`rounded-circle  hover-pointer d-flex align-items-center justify-content-center ${styles.iconWrap}`}><VideoCameraFilled /> </div>
                    </Space>
                </div>
                <Divider dashed />
                <div className='d-flex align-items-center justify-content-between action-wrap'>
                <div className='d-flex align-items-center hover-pointer'>
                <CloseOutlined className='text-danger'/>
                <span className='m-0 text-danger'>Cancel booking</span>
                </div>
                <div className='d-flex align-items-center hover-pointer'>
                <CalendarOutlined className='text-warning'/>
                <span className='m-0 text-warning'>Reschedule</span>
                </div>
                </div>

            </section>
        </>
    )
})

export { NextAppointment }
