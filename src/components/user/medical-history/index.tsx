import React, { memo } from "react"
import styles from "./styles.module.scss"
import { Steps, Card, Timeline } from "antd"
import "./styles.scss"
import { CheckOutlined, ClockCircleOutlined } from "@ant-design/icons"

const { Step } = Steps

interface MedicalHistoryProps {}

const MedicalHistory: React.FC<MedicalHistoryProps> = memo(() => {
  const renderMedicines = () => {
    return medicines.map(medicine => {
      return (
        <Timeline.Item>
          <p className={styles.time}>{medicine.time}</p>
          {medicine.pills.map(pill => {
            return (
              <Card className={` my-1 border-radius-12 ${styles.pillWrap}`}>
                <div className="d-flex  justify-content-between">
                  <div className={styles.pillInfo}>
                    <p className="m-0">{pill.name}</p>
                    <span>{pill.dosage}</span>
                  </div>
                  <div
                    className={` d-flex align-items-center justify-content-center border-radius-12 ${styles.iconWrap}`}
                  >
                    <CheckOutlined className="text-success" />
                  </div>
                </div>
                <div
                  className={`${styles.timeWrap} d-flex align-items-center mt-3 `}
                >
                  <ClockCircleOutlined className={styles.icon} />
                  <p className="m-0">{pill.time}</p>
                </div>
              </Card>
            )
          })}
        </Timeline.Item>
      )
    })
  }
  const medicines = [
    {
      time: "2021-6-5",
      pills: [
        {
          name: "Naproxen",
          dosage: "3pilss(10mg)",
          time: "8:00 - 9:00"
        },
        {
          name: "Insulin",
          dosage: "1injection(8ml)",
          time: "12:00 - 14:00"
        }
      ]
    },
    {
      time: "2021-6-12",
      pills: [
        {
          name: "Naproxen",
          dosage: "3pilss(10mg)",
          time: "8:00 - 9:00"
        },
        {
          name: "Insulin",
          dosage: "1injection(8ml)",
          time: "12:00 - 14:00"
        }
      ]
    }
  ]

  return (
    <div className={`p-3 ${styles.wrap} medical-history-wrap`}>
      <h4>Medical History</h4>
      <Timeline mode="left" className="mt-4">
        {renderMedicines()}
        <Timeline.Item></Timeline.Item>
      </Timeline>
    </div>
  )
})

export { MedicalHistory }
