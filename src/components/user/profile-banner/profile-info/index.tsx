import React, { memo } from "react"
import { userProfile } from "@assets"
import styles from "./styles.module.scss"
import { Avatar } from "antd"
import { useAuth } from "@context"

interface ProfileInfoProps { }

const ProfileInfo: React.FC<ProfileInfoProps> = memo(() => {
  const { user } = useAuth()

  return (
    <section className={`d-flex align-items-center ${styles.avatarCard}`}>
      <div>
        {
          user?.picture ? (
            <Avatar shape="square" src={user?.picture} size={170} className={styles.avatar} />
          ) :
            <Avatar shape="square" size={170} className={styles.avatar}>
              {user?.fullName?.charAt(0)}
            </Avatar>
        }
      </div>
      <div className={styles.info}>
        <h3 className="name">{user?.fullName}</h3>
        <p className="username">@{user?.username}</p>
      </div>
    </section>
  )
})

export { ProfileInfo }
