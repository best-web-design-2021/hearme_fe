import React, { CSSProperties, useCallback } from "react"
import { Avatar, Badge } from "antd"
import {
  avatar1,
  avatar2,
  avatar3,
  avatar4,
  avatar5,
  avatar6,
  avatar7
} from "@/assets"
import styles from "./styles.module.scss"
import _ from "lodash"
import { RiMoreFill } from "react-icons/ri"
import "./styles.scss"
import { useHistory } from "react-router-dom"
import { useAuth } from "@context"

interface contactProps {
  style?: CSSProperties
}

const ListContact: React.FC<contactProps> = ({}) => {
  const history = useHistory()
  const { user } = useAuth()

  const contact = [
    {
      ...user,
      name: user?.fullName,
      newMessage: 3,
      avatar: avatar1
    }
  ]

  const toMessage = useCallback(
    (username: string) => {
      history.push(`/message/${username}`)
    },
    [history]
  )

  const renderContact = () => {
    return _.map(
      contact,
      ({ fullName, newMessage, avatar, username, _id }, index) => {
        return (
          <div
            onClick={() => toMessage(username || _id)}
            className={`mt-3 ${styles.contactItemWrap} contact-item-wrap d-flex align-items-center justify-content-between`}
            key={`contact ${index}`}
          >
            <div className="d-flex align-items-center">
              <Avatar
                shape="square"
                size={40}
                className="border-radius-12"
                src={avatar}
              >
                {fullName?.charAt(0)}
              </Avatar>
              <p className={` ${styles.contactName} m-0`}>{fullName}</p>
            </div>
            {newMessage ? (
              <Badge count={newMessage} />
            ) : (
              <RiMoreFill className="hover-pointer" />
            )}
          </div>
        )
      }
    )
  }
  return (
    <>
      <section className={` mt-3 ${styles.friendListWrap} friend-list-wrap`}>
        <div className=" px-4 d-flex justify-content-between align-items-center">
          <p className="m-0 text-uppercase">contact</p>
          <Badge count={4} />
        </div>
        <div className="shadow-css bg-white border-radius-12 mt-3 p-3">
          <div>{renderContact()}</div>
        </div>
      </section>
    </>
  )
}

export default ListContact
