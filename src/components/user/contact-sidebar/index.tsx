import React, { CSSProperties } from "react"
import ListContact from "./list-contact"
import { QuickChat } from "./quick-chat"
import "./styles.scss"

interface contactProps {
  style?: CSSProperties
}

const ContactSidebar: React.FC<contactProps> = ({}) => {
  return (
    <>
      <QuickChat />
      <ListContact />
    </>
  )
}

export default ContactSidebar
