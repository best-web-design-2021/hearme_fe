import React, { memo } from "react"
import { Divider, Input } from "antd"
import { PhoneOutlined, PlusOutlined } from "@ant-design/icons"
import ScrollToBottom, {
  useScrollToBottom,
  useSticky
} from "react-scroll-to-bottom"
import styles from "./styles.module.scss"
import _ from "lodash"
import "./styles.scss"

interface MessageWrapProps {}

const MessageWrap: React.FC<MessageWrapProps> = ({}) => {
  const message = [
    {
      myMess: false,
      text: "Good day, Recently  saw properties in a great location that i did not pay attention to before."
    },
    {
      myMess: true,
      text: "Hello, have a nice day"
    },
    {
      myMess: false,
      text: "ohh, why dont you say something more"
    },
    {
      myMess: true,
      text: "He creates atmosphere of mistery"
    },
    {
      myMess: false,
      text: "Awesome! It's going to be amazing deal!"
    },
    {
      myMess: false,
      text: "I've run through different docs"
    },
    {
      myMess: false,
      text: "Hope for the best!"
    },
    {
      myMess: true,
      text: "Thanks for sending the deal, I'll review it and got back to you shortly"
    },
    {
      myMess: false,
      text: "Okey!"
    },
    {
      myMess: true,
      text: "Hey, I would like to create a training plan. Can you help me with that"
    },
    {
      myMess: true,
      text: ":))"
    },
    {
      myMess: false,
      text: "Of course, to create a perfect plan. I need information"
    }
  ]
  const scrollToBottom = useScrollToBottom()
  const [sticky] = useSticky()

  const renderMess = () => {
    return _.map(message, ({ text, myMess }) => {
      return (
        <div
          className={`d-flex  align-items-start ${
            myMess ? `flex-row-reverse` : null
          }`}
        >
          {!sticky && (
            <button onClick={scrollToBottom}>
              Click me to scroll to bottom
            </button>
          )}
          <div
            className={`${
              myMess ? `flex-column align-items-end d-flex  ` : null
            }`}
          >
            <div className={` ${styles.textMesWrap} my-2`}>
              <div style={{ maxWidth: "max-content" }}>
                <p
                  className={` px-3 py-2 m-0 ${
                    myMess ? styles.textMyMess : styles.textMess
                  }`}
                >
                  {text}
                </p>
              </div>
            </div>
          </div>
        </div>
      )
    })
  }
  return (
    <>
      <section
        className={` px-3 message-render-wrap  message-render ${styles.wrap}`}
      >
        <ScrollToBottom className={styles.scrollWrap}>
          {renderMess()}
        </ScrollToBottom>
        <div className="px-5">
          <Divider />
        </div>
        <div className="d-flex align-items-center ">
          <div
            style={{ width: "10%" }}
            className="d-flex align-items-center justify-content-center"
          >
            <div
              className={` d-flex align-items-center justify-content-center ${styles.plusIcon}`}
            >
              <PlusOutlined />
            </div>
          </div>
          <div style={{ width: "80%" }}>
            <Input placeholder="Start typing your message" bordered={false} />
          </div>
          <div
            style={{ width: "10%" }}
            className="d-flex align-items-center justify-content-center"
          >
            <PhoneOutlined className={styles.phoneIcon} />
          </div>
        </div>
      </section>
    </>
  )
}

export default memo(MessageWrap)
