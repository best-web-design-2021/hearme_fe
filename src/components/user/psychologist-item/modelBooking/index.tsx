import { GSchedule } from "@/utils"
import { Modal } from "antd"
import React, {
  forwardRef,
  memo,
  useCallback,
  useImperativeHandle,
  useState
} from "react"
import BookSuccess from "../../book-success-modal"

interface ModelBookedProps {
  // is: boolean
}

export interface ModelBookedRef {
  showModal: (data: GSchedule) => void
}

const ModelBooked = memo(
  forwardRef<ModelBookedRef, ModelBookedProps>(({}, ref) => {
    const [isModalVisible, setIsModalVisible] = useState(false)
    const [schedule, setSchedule] = useState<GSchedule>(null)

    useImperativeHandle(ref, () => ({
      showModal
    }))

    const showModal = useCallback((data: GSchedule) => {
      setSchedule(data)
      setIsModalVisible(true)
    }, [])

    const handleCancel = useCallback(() => {
      setIsModalVisible(false)
    }, [])

    return (
      <Modal
        wrapClassName="success-booking-modal"
        visible={isModalVisible}
        // onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
        width={1000}
      >
        <BookSuccess schedule={schedule} onCloseModal={handleCancel} />
      </Modal>
    )
  })
)

export default ModelBooked
