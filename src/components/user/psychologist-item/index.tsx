import React, { useCallback, useState } from "react"

import { Dropdown, Menu, Space, Drawer } from "antd"
import { DownOutlined, SearchOutlined } from "@ant-design/icons"
import _ from "lodash"
import "./styles.scss"
import styles from "./styles.module.scss"
import DoctorItem from "./doctorItem"
import { useServiceQuery } from "@/utils/hooks/graphql"
import { DOCTOR_RS } from "@/graphql"
import { GDoctor, GAppointment } from "@utils"
import { LoadingLazyComponent } from "@components/loading-page"
import { PsychologistFilter } from "../psychologist-filter"

interface PsychologistListItemProps { }

const PsychologistListItem: React.FC<PsychologistListItemProps> = () => {
  const { data, loading } = useServiceQuery<{ doctorsRS: { doctor: GDoctor, freetimes: GAppointment[] }[] }>(
    DOCTOR_RS,
    {
      options: {
        fetchPolicy: "no-cache"
      }
    }
  )
  const [drawerVisible, setDrawerVisible] = useState<boolean>(false)
  const showDrawer = () => {
    setDrawerVisible(true)
  }

  const onClose = () => {
    setDrawerVisible(false)
  }

  const menu = (
    <Menu>
      <Menu.Item key="0">
        <a href="https://www.antgroup.com">1st menu item</a>
      </Menu.Item>
      <Menu.Item key="1">
        <a href="https://www.aliyun.com">2nd menu item</a>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">3rd menu item</Menu.Item>
    </Menu>
  )

  if (loading)
    return (
      <div>
        <LoadingLazyComponent />
      </div>
    )

  return (
    <div className={`psychologist-item-wrap ${styles.wrap}`}>
      <Space align="center">
        <h1 className={`${styles.title} text-capitalize m-0`}>
          Our Therapists
        </h1>
        <SearchOutlined
          className={`d-lg-none d-md-none ${styles.searchIcon}`}
          onClick={showDrawer}
        />
      </Space>
      <div className="d-flex align-items-center justify-content-between px-3">
        <Dropdown overlay={menu} trigger={["click"]}>
          <p
            className={`${styles.sort} ant-dropdown-link m-0 `}
            onClick={e => e.preventDefault()}
          >
            sort <DownOutlined />
          </p>
        </Dropdown>
        <div className="d-flex align-items-center justify-content-center">
          <p className={`${styles.needHelp} m-0`}>Need more help?</p>
          <button className={`btn py-2  ${styles.guideMatchingButton}`}>
            Guide Matching
          </button>
        </div>
      </div>
      <div className="px-2 ">
        {
          data?.doctorsRS?.map((d, index) => (
            <DoctorItem
              doctor={d.doctor}
              avatar={null}
              freetimes={d.freetimes}
              fullName={d.doctor.user.fullName}
              username={d.doctor.user.username}
              intro={d.doctor.slogan}
              _id={d.doctor._id}
              specialties={d.doctor.specialties}
              offer={d.doctor.offeres}
              key={`doctor ${index}`}
            />
          ))
          // data?.doctorsRS?.map((d, index) => (
          //   <DoctorItem {...d} key={`doctor ${index}`} />
          // ))
        }
      </div>
      <Drawer
        placement="left"
        closable={false}
        onClose={onClose}
        visible={drawerVisible}
      >
        <PsychologistFilter />
      </Drawer>
    </div>
  )
}

export default PsychologistListItem
