import React, { memo, useCallback, useEffect, useState } from "react"
import moment from "moment"
import {
  Calendar,
  notification,
  Radio,
  Input,
  RadioChangeEvent,
  Col,
  Row,
  Spin
} from "antd"
import './styles.scss'
import styles from "./styles.module.scss"
import { InfoCircleTwoTone } from "@ant-design/icons"
import { EnumChannelSchedule, GAppointment, GDoctor, GSchedule } from "@/utils"
import { useServiceMutation } from "@/utils/hooks/graphql"
import { BOOK_SCHEDULE, RS_SCHEDULE_OF_DOCTOR } from "@/graphql"
import ButtonComponent from "@components/button"
import useServiceLazyQuery from "@/utils/hooks/graphql/useServiceLazyQuery"

interface ChooseBookProps {
  // idDoctor: string
  doctor: GDoctor
  // freetimes: GAppointment[]
  showModal: (schedule: GSchedule) => void
}

const ChooseBook: React.FC<ChooseBookProps> = memo(
  ({ doctor, showModal }) => {
    const [booking, { loading }] =
      useServiceMutation<{ bookSchedule: GSchedule }>(BOOK_SCHEDULE)

    const [getRs, { data: dataRs, loading: rsLoading }] = useServiceLazyQuery<{ rSScheduleOfDoctor: GAppointment[] }>(RS_SCHEDULE_OF_DOCTOR, {
      options: { fetchPolicy: 'no-cache' }
    })
    const [timeFilter, setTimeFilter] = useState<number>(
      moment().startOf("day").valueOf()
    )
    const [times, setTimes] = useState<[moment.Moment, moment.Moment]>([
      moment(),
      moment().add(30, "minute")
    ])
    const [channel, setChannel] = useState<EnumChannelSchedule>(
      EnumChannelSchedule.ONLINE
    )
    const [timePicker, setTimePicker] = useState<GAppointment>(null)
    useEffect(() => {
      setTimePicker(null)
      if (!doctor) return
      getRs({
        variables: {
          user_id: doctor.user._id,
          time: timeFilter
        }
      })

    }, [timeFilter, doctor])

    const renderTime = () => {
      return rsLoading ? <div className='d-flex align-items-center w-100 justify-content-center'> <Spin /> </div> : dataRs?.rSScheduleOfDoctor?.map(item => {
        return (
          <Col
            span={8}
            className='pe-2 pt-2'
          >
            <div onClick={() => {
              setTimePicker(item)
            }} className={`hover-pointer py-2 d-flex align-items-center justify-content-center ${styles.timeTag} ${timePicker?.from == item.from ? styles.timePicked : null}`}>
              <p className="m-0">{`${item.from}:00`}</p>
            </div>
          </Col>
        )
      })
    }

    const changeChannel = useCallback((e: RadioChangeEvent) => {
      setChannel(e.target.value)
    }, [])

    const disabledDate = useCallback(current => {
      return (
        (current && current < moment().endOf("day")) ||
        (current && current > moment().add(7, "day"))
      )
    }, [])

    const setTime = useCallback((value: moment.Moment) => {
      setTimeFilter(value.startOf("day").valueOf())
    }, [])

    const onBook = useCallback(async () => {
      const duration = times[1].valueOf() - times[0].valueOf()
      const timeStartDay =
        times[0].valueOf() - times[0].clone().startOf("day").valueOf()
      const time = {
        from: timePicker.from,
        to: timePicker.to,
        date: timePicker.date
      }
      try {
        const scheduleMutation = await booking({
          variables: {
            input: {
              idDoctor: doctor._id,
              channel,
              note: "",
              appointment: time
            }
          }
        })
        if (scheduleMutation.errors) {
          notification.error({
            message: "err"
          })
          return
        }
        showModal(scheduleMutation?.data?.bookSchedule)
      } catch (error) {
        console.log(error)
        notification.error({
          message: "err"
        })
        return
      }
    }, [times, channel, timeFilter, doctor, timePicker])

    return (
      <section className='booking-wrap'>
        <h1 className='text-center'>Make an appointment</h1>
        <Row >
          <Col lg={12} md={24} sm={24} className='p-2'>
            <div className='d-flex align-items-center justify-content-center'>
              <Radio.Group
                className='mb-3'
                onChange={changeChannel}
                defaultValue={EnumChannelSchedule.ONLINE}
              >
                <Radio value={EnumChannelSchedule.ONLINE}>Online</Radio>
                <Radio value={EnumChannelSchedule.OFFLINE}>Offline</Radio>
              </Radio.Group>
            </div>
            <Input.TextArea
              className='mb-3'
              autoSize={{ minRows: 3, maxRows: 5 }}
              placeholder='Write a note'></Input.TextArea>

            <Calendar
              style={{ width: '300px' }}
              headerRender={({ value, type, onChange, onTypeChange }) => {
                return (
                  <div className='text-center calendar-header-render p-2 '>
                    <div>
                      <label className='py-1 px-3 today-render'>{moment().format('MMMM YYYY')}</label>
                    </div>
                  </div>
                );
              }}
              onChange={setTime}
              disabledDate={disabledDate}
              fullscreen={false} />
          </Col>
          <Col lg={12} md={24} sm={24} className='py-2 ps-3'>
            <div style={{ width: '300px' }}>

              <section>
                <p className={`m-0 ${styles.timeLabel}`}>Select Time</p>
                <Row>
                  {renderTime()}
                </Row>
              </section>
              <div className={`d-flex align-items-center py-2 ps-2 mt-3 ${styles.durationNotiWrap}`}>
                <InfoCircleTwoTone className={styles.icon} twoToneColor="#ffc107" />
                <p className='m-0 ps-3'>A single meeting will last 60 minutes.</p>
              </div>
              <ButtonComponent
                onPress={onBook}
                className="mt-4 button"
                disabled={times.length < 1 || loading}
                loading={loading}
              >
                <label>Get Appointment</label>
              </ButtonComponent>
            </div>
          </Col>
        </Row>
      </section>
    )
  }
)

export default ChooseBook
