import { Avatar, Modal, Rate, Space, Tooltip } from "antd"
import {
  AppstoreTwoTone,
  EnvironmentOutlined,
  VideoCameraTwoTone,
  CheckOutlined
} from "@ant-design/icons"
import React, { useState, useCallback, useEffect, memo, useRef } from "react"
import ButtonComponent from "@components/button"
import styles from "../styles.module.scss"
import ModelBookSuccess, { ModelBookedRef } from "../modelBooking"
import ChooseBook from "../chooseBook"
import "../styles.scss"
import { GSchedule, GAppointment, GDoctor } from "@/utils"

interface DoctorItemProps {
  _id: string
  doctor: GDoctor
  avatar: string
  username: string
  fullName: string
  freetimes: GAppointment[]
  star?: number
  position?: string
  intro?: string
  offer?: string[]
  specialties?: string[]
  time?: string[]
  type?: string
}

const DoctorItem: React.FC<DoctorItemProps> = memo(
  ({
    _id,
    avatar,
    fullName,
    star = 2,
    doctor,
    position,
    username,
    intro,
    freetimes = [],
    offer = ["CBT", "EMDR", "ACT", "More", "More", "More", "More", "More"],
    specialties = ["Eating Disoder", "Abuse", "PTSD", "more"]
  }: any) => {
    const refModelSuccess = useRef<ModelBookedRef>(null)
    const [bookingModalVisible, setBookingModalVisible] =
      useState<boolean>(false)

    const showBookingModal = useCallback(() => {
      setBookingModalVisible(true)
    }, [])

    const onhandleCancel = useCallback(() => {
      setBookingModalVisible(false)
    }, [])

    const showModal = useCallback((schedule: GSchedule) => {
      refModelSuccess.current?.showModal(schedule)
    }, [])

    return (
      <div
        className={`${styles.doctorItemWrap} bg-white border-radius-12 shadow-css mt-4 overflow-auto w-100`}
      >
        <div className={`py-4 px-2 w-100 d-flex flex-xl-row flex-column`}>
          <div className={`${styles.basicInfo}`}>
            <div className="d-flex align-items-center flex-xl-column w-100">
              <div>
                <Avatar size={140} src={avatar}>
                  N
                </Avatar>
              </div>
              <div className='d-flex flex-column  align-items-xl-center px-xl-none px-2'>
                <p className={`m-0 ${styles.name} d-xl-none`}>{fullName}</p>
                <Space className={`pb-lg-0 pb-1 ${styles.optionWrap} `}size='middle'>
                  <AppstoreTwoTone />
                  <Tooltip placement="bottom" title='video call'>
                    <VideoCameraTwoTone />
                  </Tooltip>
                </Space>
                <Rate value={star}  disabled />
              </div>
            </div>
          </div>
          <div className='px-3'>
            <div className='d-flex align-items-center justify-content-between'>
              <div>
                <p className={`m-0 d-xl-block d-none ${styles.name}`}>
                  {fullName}
                </p>
                <p className={`m-0 d-xl-none  ${styles.name}`}>
                  location
                </p>
                <div className={`d-flex align-items-center ${styles.location}`}><EnvironmentOutlined className={styles.locationIcon} /><p className={`text-capitalize m-0 `}>
                  310 Nguyễn Văn cừ
                </p>
                </div>

              </div>
              <div>

                <p
                  className={`text-capitalize m-0 ${styles.priceTitle}`}
                >
                  Approximate price:
                </p>
                <p className={`m-0  text-right ${styles.price}`}>
                  30$
                </p>
              </div>
            </div>
            <div className={` pt-2 ${styles.intro}`}>
              <p className="m-0 ">{intro} Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore aperiam ullam libero dignissimos culpa quam? Autem fugiat inventore animi officiis repellendus dolor laborum. Distinctio laborum explicabo libero, facilis cupiditate iste dicta amet vel sunt dolor deserunt rerum nobis odit dolorem, tenetur ab illum. Facere nobis in quis quaerat iusto harum.</p>
            </div>

            <div className={`pt-3 ${styles.therapiesOffered}`}>
              <p className="m-0">Therapies Offered</p>
              <div className=" pt-2 pb-3">
                {offer.map(offerItem => {
                  return (
                    <div
                      className={`mx-1 mt-1 d-inline-block ${styles.tagItem} shadow-css py-2 px-3`}
                    >
                      {offerItem}
                    </div>
                  )
                })}
              </div>
            </div>
            <div className={styles.special}>
              <p className="m-0">Top Specialties</p>
              <div className=" pt-2 pb-3">
                {specialties.map(specialtiesItem => {
                  return (
                    <div
                      className={`mx-1 mt-1 ${styles.tagItem} d-inline-block shadow-css py-2 px-3`}
                    >
                      {specialtiesItem}
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='d-flex  justify-content-end'>
              <ButtonComponent className={`px-3 ${styles.button}`} onPress={showBookingModal}>
                <CheckOutlined />
                <p className="m-0">Book</p>
              </ButtonComponent>
            </div>
          </div>

          {/* <div className={`p-3 d-none d-xl-block  ${styles.timePicker} `}>
              <ChooseBook idDoctor={_id} showModal={showModal} />
            </div>
            <div className="d-xl-none">
              <ButtonComponent onPress={showBookingModal}>
                <p className="m-0">book</p>
              </ButtonComponent>
            </div> */}


        </div>
        <Modal
          visible={bookingModalVisible}
          onCancel={onhandleCancel}
          footer={null}
          width={'max-content'}
          wrapClassName="booking-modal"
        
        >
          <div className={`p-3  ${styles.timePicker} `}>
            <ChooseBook doctor={doctor} showModal={showModal} />
          </div>
        </Modal>
        <ModelBookSuccess ref={refModelSuccess} />
      </div>
    )
  }
)

export default DoctorItem
