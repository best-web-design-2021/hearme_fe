import React, { memo } from 'react';
import { Divider, Menu } from 'antd'
import { Link } from 'react-router-dom'

interface MenuProps {
    mR: any
}

const MenuSider: React.FC<MenuProps> = ({
    mR
}: any) => {
    return (
        <>
            {mR.isMenu ? (
                <>
                    <Menu.Item
                        key={`menu ${mR.path}`}
                        icon={mR.icon}
                        className="py-4"
                    >
                        <Link to={mR.path}> {mR.title}</Link>
                    </Menu.Item>
                    <div className="px-4">
                        <Divider className="m-0" />
                    </div>
                </>
            ) : null}
        </>
    )
}

export default memo(MenuSider);