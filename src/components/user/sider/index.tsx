import React from "react"
import styles from "./styles.module.scss"
import "./sider.scss"
import ButtonComponent from "@components/button"
import { Avatar, Badge, Menu, Divider } from "antd"
import { CloseOutlined, HomeOutlined } from "@ant-design/icons"
import { useAuth } from "@context"
import { routersUser } from "@routers"
import { Link, useRouteMatch } from "react-router-dom"

interface SiderComponentProps { }

const SiderComponent: React.FC<SiderComponentProps> = () => {
  const { user } = useAuth()
  const { path } = useRouteMatch()

 

  return (
    <div className={` user-sider-wrap ${styles.container}`}>
      {/* user name */}
      <section className="py-4 px-3">
        <div
          className={` ${styles.userNameWrap}  shadow-css d-flex align-items-center bg-white p-4`}
        >
          {
            user?.picture ? (
              <Avatar shape="square" src={user?.picture} size={40} className="border-radius-12" />
            ) :
              <Avatar shape="square" size={40} className="border-radius-12">
                {user?.fullName?.charAt(0)}
              </Avatar>
          }
          <div className="px-3">
            <p className={`m-0 ${styles.name}`}>{user.fullName}</p>
            <p className={`m-0 text-muted ${styles.username}`}>
              @{user.username.substr(0, 10).length < user.username.length ? user.username.substr(0, 10) + "..." : user.username}
            </p>
          </div>
        </div>
      </section>
      {/* SIDER-MENU */}
      <section className="px-3">
        <Menu
          className="border-radius-12 py-4 shadow-css"
          defaultSelectedKeys={[`menu ${path}`]}
          mode="inline"
        // theme="dark"
        >
          {routersUser.map((mR, index) => {
            return (
              mR.isMenu ? (
                  <Menu.Item
                  className='h-100'
                    key={`menu ${mR.path}`}
                  >
                    <Link to={mR.path}>
                    <div className={`py-2 ${styles.menuItemWrap} menu-item-wrap`}>
                    <div  className='d-flex align-items-center h-100' >{mR.icon} <p className='m-0'> {mR.title}</p></div>
                    </div>
                    </Link>
                  </Menu.Item>
              ) : null
            )
          })}
        </Menu>
      </section>
      {/* INVITATION */}
      <section
        className={` ${styles.friendRequestWrap} friend-request-wrap  py-4 px-3`}
      >
        <div className="px-4 d-flex align-items-center justify-content-between">
          <p className={`m-0 text-uppercase ${styles.friendRequestTitle}`}>
            friend requests
          </p>
          <Badge count={4} />
        </div>
        <div className="py-3">
          <div
            className={` ${styles.friendRequestContainer} friend-request-container border-radius-12 shadow-css bg-white p-4`}
          >
            <div className="d-flex align-items-center">
              <div>
                <Avatar
                  style={{ backgroundColor: "#f56a00" }}
                  shape="square"
                  size={40}
                  className="border-radius-12 mr-2"
                >
                  Q
                </Avatar>
              </div>
              <p className="m-0">
                <span>MVQuang </span>wants to add you to friends
              </p>
            </div>
            <div className="d-flex align-items-center pt-3">
              <div className="button-wrap pr-1">
                <ButtonComponent children="Accept" />
              </div>
              <div>
                <CloseOutlined className="flex-grow-1" />
              </div>
            </div>
          </div>
        </div>
      </section>
    </div >
  )
}

export default SiderComponent
