import React, { memo } from "react"
import styles from './styles.module.scss'
import './styles.scss'
import { Card, Checkbox, Typography } from 'antd'
import { BellOutlined, BellTwoTone } from '@ant-design/icons'

const { Text } = Typography
interface DailyMedProps { }

const medOption = [
    {
      label: "1x Cilatopram(20mg)",
      note: "Take with food in the morning",
      isChecked: true
    },
    {
      label: "2x Lisinnopril(10mg)",
      note: "Take twice daily with water",
      isChecked: false
    },
    {
      label: "1x loratadine(10mgs)",
      note: "Taken once per day",
      isChecked: false
    }
  ]
const DailyMed: React.FC<DailyMedProps> = memo(() => {
    return (
        <div className={styles.wrap}>
           {
               medOption.map(item => {
                   return (
                    <Card
                    className={` ${
                      item.isChecked ? styles.cardChecked : null
                    } w-100 medi-card border-radius-12 my-2`}
                  >
                    <Checkbox
                      value={item.label}
                      checked={item.isChecked}
                      style={{ width: "90%" }}
                    >
                      <div className={styles.text}>
                        <Text delete={item.isChecked ? true : false}>
                          <p className="m-0">{item.label}</p>
                        </Text>
                        <Text
                          delete={item.isChecked ? true : false}
                          className={styles.mediNote}
                        >
                          {item.note}
                        </Text>
                      </div>
                    </Checkbox>
                    <div
                      style={{ width: "10%" }}
                      className="d-flex align-items-center justify-content-center"
                    >
                      {item.isChecked ? (
                        <BellOutlined className={styles.bellOutline} />
                      ) : (
                        <BellTwoTone />
                      )}
                    </div>
                  </Card>
                   )
             
               })
           }
        </div>
    )
})

export { DailyMed }
