import React, { memo, useCallback, useEffect } from "react"
import { Row, Col, notification } from "antd"
import { doctorWoman, firework } from "@assets"
import styles from "./styles.module.scss"
import OutlineButton from "@components/outline-button"
import ButtonComponent from "@components/button"
import { appointmentToTime, EnumChannelSchedule, GSchedule } from "@/utils"
import moment from "moment"
import { useServiceMutation } from "@/utils/hooks/graphql"
import { CANCEL_SCHEDULE } from "@/graphql"

interface bookSuccessProps {
  schedule: GSchedule,
  onCloseModal?: () => void
}

const BookSuccess: React.FC<bookSuccessProps> = memo(({ schedule, onCloseModal }) => {
  
  const [cancel, { loading: canceling, data }] = useServiceMutation<{ cancelSchedule: GSchedule }>(CANCEL_SCHEDULE)

  useEffect(() => {
    if (!data) return
    if (!data?.cancelSchedule) {
      notification.error({
        message: 'Không thể hủy lịch'
      })
      return
    }
    notification.success({
      message: 'Hủy lịch thành công'
    })
    onCloseModal && onCloseModal()
  }, [data])

  const cancelSchedule = useCallback(() => {
    cancel({
      variables: {
        id: schedule._id
      }
    })
  }, [schedule])

  const time = appointmentToTime(schedule.appointment)

  return (
    <Row className={styles.wrap}>
      <Col span={13}>
        <div
          className="d-flex align-items-center justify-content-center position-relative"
          style={{
            backgroundImage: `url(${doctorWoman})`,
            backgroundSize: "cover",
            width: "100%",
            height: "500px"
          }}
        >
          <div
            className={`bg-white d-flex align-items-center shadow-css-xl p-3 border-radius-12 mt-5 position-absolute  ${styles.appointmentDetailWrap}`}
          >
            <div className={`px-3 ${styles.appoitmentDetail}`}>
              <p className={`m-0 ${styles.label}`}>Appointment detail</p>
              <p className={`m-0 ${styles.content}`}>
                {moment(time.date).format("MMM, DD YYYY")}
                {` ${moment(time.from).format("HH:mm")} - ${moment(time.to).format("HH:mm")}`}
              </p>
            </div>
            <div className="px-3">
              <p className={`m-0 ${styles.label}`}>Service</p>
              <p className={`m-0 ${styles.content}`}>
                {schedule?.channel === EnumChannelSchedule.ONLINE
                  ? "Online"
                  : "Offline"}
              </p>
            </div>
            <div className="">
              <p className={`m-0 ${styles.label}`}>Professional</p>
              <p className={`m-0 ${styles.content}`}>
                {schedule?.doctor?.user?.fullName}
              </p>
            </div>
          </div>
        </div>
      </Col>
      <Col span={11} className="d-flex  px-4">
        <section className={styles.mainContent}>
          <div className={styles.fireWork}>
            <img src={firework} />
          </div>
          <h2 style={{ zIndex: 550, position: "relative" }}>
            Appointment booked successfully!
          </h2>
          <p className={`py-2 pr-4 ${styles.textDetail}`}>
            Your appointment details are shown. You can add the booking to your
            favorite calendar
          </p>
          <div className={`d-flex align-items-center ${styles.buttonWrap}`}>
            <OutlineButton className={styles.cancelButton} onPress={cancelSchedule} >
              <p className="m-0">{ canceling ? 'Canceling' : 'Cancel booking'}</p>
            </OutlineButton>
            <ButtonComponent className={styles.viewDetail}>
              <p className="m-0">View detail</p>
            </ButtonComponent>
          </div>
        </section>
      </Col>
    </Row>
  )
})

export default BookSuccess
