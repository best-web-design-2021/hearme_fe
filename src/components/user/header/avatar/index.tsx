import React, { CSSProperties, memo, useCallback } from "react"
import { Dropdown, Avatar, Menu, Divider } from "antd"
import { useHistory } from "react-router-dom"
import "./styles.scss"
import { useAuth } from "@/context"
import { UserOutlined, HeartOutlined, LogoutOutlined } from "@ant-design/icons"
import { mutateData } from "@/tools"
import { LOGOUT } from "@/graphql"
import { ACCESS_TOKEN } from "@/constants"

interface avatarProps {
  style?: CSSProperties
}

const AvatarComponent: React.FC<avatarProps> = ({ }) => {
  const history = useHistory()
  const { user, dispatchAuth } = useAuth()

  const click = useCallback(() => {
    history.push(`/profile/${user?.username || user?._id}`)
  }, [history])

  const press = useCallback(path => {
    history.push(`/${path}`)
  }, [])

  const logout = useCallback(async () => {
    try {
      const { data, errors } = await mutateData(LOGOUT)
      if (errors || !data?.logout) {
        return
      }
    } catch (error) { }
    localStorage.removeItem(ACCESS_TOKEN)
    dispatchAuth({
      type: "SET_AUTHEN",
      payload: {
        isAuth: false
      }
    })
    history.replace("/")
  }, [history, dispatchAuth])

  const menuItem = [
    {
      key: `profile/${user?.username || user?._id}`,
      title: "View Profile",
      icon: <UserOutlined />
    },
    {
      key: "become-a-therapist",
      title: "Become a Therapist",
      icon: <HeartOutlined />
    }
  ]
  const menu = () => {
    return (
      <Menu className={"header-avatar-menu border-radius-12 py-2"}>
        {menuItem.map(item => {
          return (
            <Menu.Item
              className="py-2"
              icon={item.icon}
              onClick={() => press(item.key)}
            >
              {item.title}
            </Menu.Item>
          )
        })}

        <Menu.Item icon={<LogoutOutlined />} onClick={logout} className="py-2">
          Logout
        </Menu.Item>
      </Menu>
    )
  }

  return (
    <div className="header-avatar-wrap">
      <Dropdown
        overlay={menu}
        placement="bottomRight"
        className="hover-pointer notification-wrap-header"
      >
        {
          user?.picture ? (
            <Avatar shape="square" src={user?.picture} size={40} className="border-radius-12" />
          ) :
            <Avatar shape="square" size={40} className="border-radius-12">
              {user?.fullName?.charAt(0)}
            </Avatar>
        }
      </Dropdown>
    </div>
  )
}

export default memo(AvatarComponent)
