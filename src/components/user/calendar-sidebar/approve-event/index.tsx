import React, { forwardRef, memo, useEffect, useImperativeHandle, useState } from "react"
import styles from "./styles.module.scss"
import { Badge } from "antd"
import "./styles.scss"
import { EventItem } from './event-item'
import { EnumEvent, GSchedule } from "@utils"
import { LoadingLazyComponent } from "@components/loading-page"
import { useServiceQuery } from "@/utils/hooks/graphql"
import { MY_SCHEDULES_APPROVE } from "@/graphql"

interface ApproveEventProps {
  onSchedulesChange: (schedules: GSchedule[]) => void
}

export interface ApproveEventRef {
  setState?: (schedules: GSchedule[]) => void
  refetch: () => void
}

const ApproveEvent = memo(forwardRef<ApproveEventRef, ApproveEventProps>(({
  onSchedulesChange
}, ref) => {

  const { loading, data, refetch } = useServiceQuery<{ myBookSchedulesApprove: GSchedule[] }>(MY_SCHEDULES_APPROVE,
    {
      options: {
        fetchPolicy: "no-cache"
      }
    })
  const [dataSoket, setDataSoket] = useState<GSchedule[]>([]);

  useEffect(() => {
    if (!data) return
    onSchedulesChange(data?.myBookSchedulesApprove || [])
  }, [data])

  useImperativeHandle(ref, () => ({
    refetch
  }))

  return (
    <section className="approve-event">
      <div className="px-3 d-flex align-items-center justify-content-between">
        <p className={`m-0 text-uppercase ${styles.title}`}>awaiting task</p>
        {
          data?.myBookSchedulesApprove?.length > 0 && <Badge count={data.myBookSchedulesApprove.length} />
        }
      </div>
      <div className={`${ data?.myBookSchedulesApprove?.length > 0 ? styles.eventItemWrap : styles.noEventWrap}`}>
        {
          loading ? (
            <div className='px-2'>
              <LoadingLazyComponent />
            </div>
          ) :
            dataSoket.concat(data?.myBookSchedulesApprove || []).map(s => (<EventItem refetch={refetch} schedule={s} key={s._id} />))
        }
      </div>
    </section>
  )
}))

export { ApproveEvent }
