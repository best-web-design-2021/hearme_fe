import React, { memo, useCallback, useEffect } from "react"
import { ClientDoctor, EnumStatusSchedule, GSchedule, EnumChannelSchedule, appointmentToTime } from "@utils"
import moment from "moment"
import { useServiceMutation } from "@/utils/hooks/graphql"
import { CANCEL_SCHEDULE, CONFIRM_SCHEDULE, DENIED_SCHEDULE } from "@/graphql"
import { Spin } from 'antd'
import styles from "../styles.module.scss"
import "../styles.scss"

interface EventItemProps {
  schedule: GSchedule
  refetch: () => void
}

const EventItem: React.FC<EventItemProps> = memo(({
  schedule,
  refetch
}) => {

  const [confirm, { loading, data }] = useServiceMutation<{ confirmSchedule: GSchedule }>(CONFIRM_SCHEDULE)
  const [denied, { loading: ldDenied }] = useServiceMutation(DENIED_SCHEDULE)
  const [cancel, { loading: ldCancel }] = useServiceMutation(CANCEL_SCHEDULE)


  useEffect(() => {
    if (!data) return
    refetch()
  }, [data])

  const confirmSchedule = useCallback(() => {
    confirm({
      variables: { id: schedule._id }
    })
  }, [schedule])

  const deniedSchedule = useCallback(async () => {
    await denied({
      variables: { id: schedule._id }
    })
    refetch()
  }, [schedule])

  const cancelSchedule = useCallback(async () => {
    await cancel({
      variables: { id: schedule._id }
    })
    refetch()
  }, [schedule])

  const time = appointmentToTime(schedule.appointment)

  return (

    <section className={`bg-white shadow-css mb-2 border-radius-12 d-flex justify-content-between `}>
      <div className="py-4">
        <div className={`${styles.nameWrap} ${schedule?.channel === EnumChannelSchedule.ONLINE ? styles.onlineChannel : styles.offlineChannel}`}>
          <h5 className={`m-0 ${styles.name}`}>{
            schedule?.isMe === ClientDoctor.CLIENT ? schedule?.doctor?.user?.fullName : schedule?.client?.fullName
          }</h5>
          <p className="m-0">{schedule?.channel}</p>
        </div>
        <p className={`m-0 pt-3 ${styles.time}`}>{moment(time.from).format("MMM, DD YYYY")}: {moment(time.from).format("HH:mm")} -{" "}
          {moment(time.to).format("HH:mm")}</p>
      </div>
      <div>
        {
          schedule?.isMe === ClientDoctor.CLIENT && schedule?.status === EnumStatusSchedule.WAITING_DOCTOR_CONFIRM ?
            (
              <>
                <div
                  className={`h-50 d-flex align-items-center justify-content-center ${styles.button}`}
                >
                  <p className="m-0">
                    {loading ? <Spin /> : "Waitting"}
                  </p>
                </div>
                <div
                  className={`h-50 d-flex align-items-center justify-content-center ${styles.button}`}
                  onClick={cancelSchedule}
                >
                  <p className="m-0">
                    {ldCancel ? <Spin /> : "Cancel"}
                  </p>
                </div>
              </>
            ) :
            (
              <>
                <div
                  className={`h-50 d-flex align-items-center justify-content-center ${styles.button}`}
                  onClick={confirmSchedule}
                >
                  <p className="m-0">{loading ? <Spin /> : "Approve"}</p>
                </div>
                <div
                  className={`h-50 d-flex align-items-center justify-content-center ${styles.button}`}
                  onClick={deniedSchedule}
                >
                  <p className="m-0">
                    {ldDenied ? <Spin /> : "Denied"}
                  </p>
                </div>
              </>
            )
        }

      </div>
    </section>


  )
})

export { EventItem }
