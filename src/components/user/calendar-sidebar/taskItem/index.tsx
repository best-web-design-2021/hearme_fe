import { appointmentToTime, ClientDoctor, EnumChannelSchedule, GSchedule } from "@utils"
import {
  AppstoreOutlined,
  MessageOutlined,
  VideoCameraOutlined
} from "@ant-design/icons"
import { Divider, Modal, Spin } from "antd"
import React, { memo, useCallback, useState } from "react"
import { MedicalHistory } from "../../medical-history"
import styles from "./styles.module.scss"
import moment from "moment"
import { useServiceQuery } from "@/utils/hooks/graphql"
import { GEN_TOKEN_MEETING } from "@graphql"

interface TaskItemProps {
  schedule: GSchedule
}

const setIcon = (type: string) => {
  switch (type) {
    case "online":
      return (
        <div className={styles.message}>
          {" "}
          <AppstoreOutlined
            className={`p-3 text-warning ${styles.messageIcon}`}
          />{" "}
        </div>
      )
    case "offline":
      return (
        <div className={styles.video}>
          {" "}
          <VideoCameraOutlined
            className={`p-3  ${styles.videoIcon}`}
          />{" "}
        </div>
      )
  }
}
const setColor = (type: string) => {
  switch (type) {
    case "online":
      return styles.borderChat
    case "offline":
      return styles.borderVideo
  }
}

const TaskItem: React.FC<TaskItemProps> = memo(({ schedule }) => {

  const [isModalVisible, setIsModalVisible] = useState<boolean>(false)
  const [loading, setLoading] = useState<boolean>(false);
  const { refetch } = useServiceQuery<{ genTokenMeeting: { token: string, userId: string } }>(GEN_TOKEN_MEETING, {
    options: { skip: true, variables: { id: schedule._id } },
  })

  const showModal = () => {
    setIsModalVisible(true)
  }

  const handleOk = () => {
    setIsModalVisible(false)
  }

  const handleCancel = () => {
    setIsModalVisible(false)
  }

  const openMeetingHandle = useCallback(async () => {
    // if (schedule.time < moment().valueOf()) return
    setLoading(true)
    try {
      const data = await refetch()
      window.open(`${process.env.MEETING_URL}/join?roomId=${schedule.code}&token=${data?.data?.genTokenMeeting?.token}`)
    } catch (error) {
      console.log(error)
    } finally {
      setLoading(false)
    }
  }, [schedule])

  const time = appointmentToTime(schedule.appointment)

  return (
    <div>
      <div className="d-flex justify-content-between">
        <div>
          <div
            className={`${setColor(
              schedule.channel === EnumChannelSchedule.ONLINE ? "online" : "offline"
            )} ${styles.name}`}
          >
            <h5 className="m-0">
              {
                schedule?.isMe === ClientDoctor.CLIENT ? schedule?.doctor?.user?.fullName : schedule?.client?.fullName
              }
            </h5>
          </div>
          <p className={styles.time}>
            {moment(time.from).format("MMM, DD YYYY")}
          </p>
          <p className={styles.time}>
            {moment(time.from).format("HH:mm")} -{" "}
            {moment(time.to).format("HH:mm")}
            minutes
          </p>
          <div
            className={`d-flex align-items-center text-uppercase ${styles.viewDetail}`}
          >
            <p className="m-0 hover-pointer" onClick={showModal}>
              medical history
            </p>
            <p className="m-0">reports</p>
          </div>
        </div>
        <div className={styles.icon} onClick={openMeetingHandle}>
          {
            loading ? <Spin /> : setIcon(
              schedule.channel === EnumChannelSchedule.ONLINE ? "online" : "offline"
            )
          }
        </div>
      </div>
      <div className="px-3">
        <Divider dashed={true} />
      </div>
      <Modal
        footer={null}
        wrapClassName="medical-view-modal"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <MedicalHistory />
      </Modal>
    </div>
  )
})

export default TaskItem
