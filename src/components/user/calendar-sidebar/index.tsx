import { reducer } from "@/common"
import { socket } from "@/config"
import { EnumEvent, GSchedule } from "@/utils"
import { notification } from "antd"
import React, { forwardRef, memo, useCallback, useEffect, useImperativeHandle, useReducer, useRef, useState } from "react"
import { ApproveEvent, ApproveEventRef } from "./approve-event"
import { UpcomingTask, UpcomingTaskRef } from "./upcoming-task"

interface CalendarSiderProps {
  onSchedulesChange: (schedules: GSchedule[]) => void
}

interface CalendarSiderState {
  approves?: GSchedule[]
  upcomings?: GSchedule[]
}

export interface CalendarSiderRef {
  schedules: GSchedule[]
}

const CalendarSider = memo(forwardRef<CalendarSiderRef, CalendarSiderProps>(({
  onSchedulesChange
}, ref) => {

  const approveRef = useRef<ApproveEventRef>(null)
  const upcommingRef = useRef<UpcomingTaskRef>(null)

  const [state, setState] = useReducer<
    (prev: CalendarSiderState, current: CalendarSiderState) => CalendarSiderState
  >(reducer, {
    approves: [],
    upcomings: []
  })

  useEffect(() => {
    socket.on(EnumEvent.NEW_SCHEDULES, (data) => {
      approveRef.current?.refetch()
      notification.success({
        message: "Có người vừa đặt lịch khám đến bạn"
      })
    })

    socket.on(EnumEvent.DENIED_SCHEDULE, (data) => {
      approveRef.current?.refetch()
      notification.error({
        message: data
      })
    })

    socket.on(EnumEvent.ACCEPTED_SCHEDULE, (data) => {
      approveRef.current?.refetch()
      notification.success({
        message: data
      })
    })

  }, [])

  useEffect(() => {
    onSchedulesChange([].concat(state.approves).concat(state.upcomings))
  }, [state])

  const approveChange = useCallback((schedules: GSchedule[]) => {
    setState({
      approves: schedules
    })
  }, [])

  const upcomingChange = useCallback((schedules: GSchedule[]) => {
    setState({
      upcomings: schedules
    })
  }, [])
  return (
    <div className="pt-3">
      <div>
        <div className="py-3">
          <ApproveEvent ref={approveRef} onSchedulesChange={approveChange} />
        </div>
        <div className="py-3">
          <UpcomingTask ref={upcommingRef} onSchedulesChange={upcomingChange} />
        </div>
      </div>
    </div>
  )
}))

export { CalendarSider }
