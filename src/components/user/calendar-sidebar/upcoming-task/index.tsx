import { GSchedule } from "@/utils"
import React, { forwardRef, memo, useEffect, useImperativeHandle } from "react"
import TaskItem from "../taskItem"
import { Empty } from "antd"
import styles from "./styles.module.scss"
import "./styles.scss"
import { LoadingLazyComponent } from "@/components/loading-page"
import { useServiceQuery } from "@/utils/hooks/graphql"
import { MY_SCHEDULES_UPCOMING } from "@/graphql"
interface UpcomingTaskProps {
  onSchedulesChange: (schedules: GSchedule[]) => void
}

export interface UpcomingTaskRef {
  // schedules: GSchedule[]
}

const UpcomingTask = memo(forwardRef<UpcomingTaskRef, UpcomingTaskProps>(({
  onSchedulesChange
}, ref) => {

  const { loading, data } = useServiceQuery<{ myBookSchedulesUpcoming: GSchedule[] }>(MY_SCHEDULES_UPCOMING,
    {
      options: {
        fetchPolicy: "no-cache"
      }
    })


  useEffect(() => {
    if (!data) return
    onSchedulesChange(data?.myBookSchedulesUpcoming || [])
  }, [data])

  // useImperativeHandle(ref, () => ({
  //   schedules: data?.myBookSchedulesUpcoming || []
  // }))
  const emptyState = () => {
    return (
      <div className={`pt-3 ${styles.emptyState}`}>
      <h3 className='text-center'>Opps!!!</h3>
      <p className={`m-0 `}>Opps, there is no upcoming appointment right now</p>
      </div>
    )
  }

  return (
    <div className='px-3'>
      <p className={`${styles.title} text-uppercase m-0 pb-2 `}>
        upcoming appointments
      </p>
      <div
        className={`${styles.upcomingTaskWrap} bg-white shadow-css border-radius-12 pt-4 pb-2`}
      >
        {loading ? (
          <div className='px-2'>
            <LoadingLazyComponent />
          </div>
        ) : (
          data?.myBookSchedulesUpcoming?.map(schedule => (
            <TaskItem schedule={schedule} key={schedule._id} />
          ))
        )}
        {
          data?.myBookSchedulesUpcoming?.length <= 0 && <Empty description={emptyState()}/>
        }
      </div>
    </div>
  )
}
))

export { UpcomingTask }
