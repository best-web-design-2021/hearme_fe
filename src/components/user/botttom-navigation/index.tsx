import React, { useState } from "react"
import { Drawer } from "antd"
import { useHistory } from "react-router-dom"
import BottomNavigation from "@material-ui/core/BottomNavigation"
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction"
import { UserOutlined, HomeOutlined, ContactsOutlined, HeartOutlined } from "@ant-design/icons"
import { BiHealth } from "react-icons/bi"
import ListContact from "../contact-sidebar/list-contact"
import './styles.scss'

// import './styles.scss'

interface bottomNavProps {}

const BottomNav: React.FC<bottomNavProps> = () => {
  const history = useHistory()
  const [visible, setVisible] = useState(false)
  const showDrawer = () => {
    setVisible(true)
  }
  const onClose = () => {
    setVisible(false)
  }
  const [value, setValue] = React.useState(0)
  
  const onHome = () => {
    history.push('/')
  }
  return (
    <>
      <Drawer
        placement="right"
        closable={false}
        onClose={onClose}
        visible={visible}
      >
        <div className="p-2">
          <ListContact />
        </div>
      </Drawer>
      <BottomNavigation
        value={value}
        onChange={(event, newValue) => {
          setValue(newValue)
          console.log(newValue)
        }}
        className={`d-lg-none position-fixed bottom-0 w-100 user-bottom-nav`}
      >
       
        <BottomNavigationAction
          onClick={showDrawer}
          icon={<ContactsOutlined />}
        />
        <BottomNavigationAction icon={<HeartOutlined />} />
        <BottomNavigationAction
          onClick={onHome}
          value="recents"
          icon={<HomeOutlined />}
        />
        <BottomNavigationAction icon={<UserOutlined />} />
        <BottomNavigationAction icon={<UserOutlined />} />
      </BottomNavigation>
    </>
  )
}

export default BottomNav
