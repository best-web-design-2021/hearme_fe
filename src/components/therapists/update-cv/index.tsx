import React, { memo, useCallback, useState } from "react"
import styles from "./styles.module.scss"
import "./styles.scss"
import ImgCrop from "antd-img-crop"
import {
  CameraOutlined,
  LoadingOutlined,
  UploadOutlined
} from "@ant-design/icons"
import {
  Form,
  Input,
  Row,
  Col,
  DatePicker,
  Space,
  Upload,
  Select,
  notification
} from "antd"
import ButtonComponent from "@components/button"
import OutlineButton from "@/components/outline-button"
import { useForm } from "antd/lib/form/Form"
import { REGIS_DOCTOR } from "@/graphql"
import { useMutation } from "@apollo/client"
import moment from "moment"
import { useAuth } from "@/context"

const { TextArea } = Input

interface UpdateCVComponentProps {
  onFinish: Function
}

const UpdateCVComponent: React.FC<UpdateCVComponentProps> = memo(
  ({ onFinish }) => {
    const [regis, { loading, data, error }] = useMutation(REGIS_DOCTOR)
    const [imageUrl, setImageUrl] = useState()
    const [loadings, setLoading] = useState<boolean>(false)
    const [form] = useForm()
    const { user } = useAuth()

    function getBase64(img, callback) {
      const reader = new FileReader()
      reader.addEventListener("load", () => callback(reader.result))
      reader.readAsDataURL(img)
    }

    const onChange = info => {
      getBase64(info.file.originFileObj, imageUrl => {
        setImageUrl(imageUrl)
        setLoading(false)
      })
    }

    const renderUploadButton = () => {
      return <div>{loadings ? <LoadingOutlined /> : <CameraOutlined />}</div>
    }
    const onPreview = async file => {
      let src = file.url
      if (!src) {
        src = await new Promise(resolve => {
          const reader = new FileReader()
          reader.readAsDataURL(file.originFileObj)
          reader.onload = () => resolve(reader.result)
        })
      }
      const image = new Image()
      image.src = src
      const imgWindow = window.open(src)
      imgWindow.document.write(image.outerHTML)
    }

    const submit = useCallback(async (values: any) => {
      // console.log(values)
      // return
      const { data, errors } = await regis({
        variables: {
          input: {
            ...values,
            cvLink: "",
            exprience: values?.exprience?.valueOf() || null
          }
        }
      })
      if (errors) {
        notification.error({
          message: "err"
        })
        return
      }
      if (data?.registerDoctor) {
        onFinish && onFinish()
      }
    }, [])

    const disableDate = useCallback(current => {
      return current > moment().endOf("day")
    }, [])

    return (
      <div className={`border-radius-12 p-3 upload-cv-wrap ${styles.wrap}`}>
        <Form onFinish={submit} name="basic" form={form}>
          <div className="d-lg-flex d-md-flex w-100">
            <div className="d-flex  justify-content-center">
              <Form.Item className="d-inline">
                <ImgCrop rotate>
                  <Upload
                    listType="picture-card"
                    showUploadList={false}
                    action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                    onChange={onChange}
                    className="rounded-circle avavtar-upload"
                  >
                    {imageUrl && loadings == false ? (
                      <img
                        src={imageUrl}
                        alt="avatar"
                        style={{ width: "100%" }}
                      />
                    ) : (
                      renderUploadButton()
                    )}
                  </Upload>
                </ImgCrop>
              </Form.Item>
            </div>
            <div className={`${styles.formWrap} w-100`}>
              <Row className="first-child-row">
                <Col lg={12} md={16} sm={16} xs={24}>
                  <Form.Item label="Name" className="d-inline">
                    <Input
                      size="large"
                      className={`w-100 ${styles.input} `}
                      placeholder="Please input your name"
                      disabled
                      value={user.fullName}
                    />
                  </Form.Item>
                </Col>
                <Col
                  lg={12}
                  md={8}
                  sm={8}
                  xs={24}
                  className="px-lg-3 px-md-2 px-sm-1"
                >
                  <Form.Item
                    label="Exprience"
                    className="d-inline "
                    name="exprience"
                  >
                    <DatePicker
                      size="large"
                      className={styles.input}
                      disabledDate={disableDate}
                    />
                  </Form.Item>
                </Col>
              </Row>
              <Form.Item label="Intro" className="d-inline" name="slogan">
                <TextArea
                  placeholder="Please input your intro"
                  allowClear
                  className={styles.input}
                />
              </Form.Item>

              <Form.Item label="Upload your CV" className="d-inline" name="cv">
                <Upload.Dragger
                  name="file"
                  action={`${process.env.GRAPHQL_URN}/upload-file`}
                  className={styles.input}
                >
                  <p className="ant-upload-drag-icon">
                    <UploadOutlined />
                  </p>
                  <p className="ant-upload-text">
                    Click or drag file to upload your cv
                  </p>
                  <p className="ant-upload-hint">
                    Support for a single or bulk upload.
                  </p>
                </Upload.Dragger>
              </Form.Item>
              <Form.Item
                label="Therapies Offered"
                className="d-inline"
                name="offeres"
              >
                <Select
                  mode="tags"
                  style={{ width: "100%" }}
                  placeholder="Therapies Offered"
                  size="large"
                  className={styles.input}
                ></Select>
              </Form.Item>
              <Form.Item
                label="Top Specialties"
                className="d-inline"
                name="specialties"
              >
                <Select
                  mode="tags"
                  style={{ width: "100%" }}
                  placeholder="Top Specialties"
                  size="large"
                  className={styles.input}
                ></Select>
              </Form.Item>
              <Form.Item>
                <Space
                  className={`${styles.buttonWrap} pt-3 w-100 d-flex align-items-center justify-content-lg-end`}
                >
                  <OutlineButton>
                    <p className="m-0 px-3">Cancel</p>
                  </OutlineButton>
                  <ButtonComponent type="submit" className="" loading={loading}>
                    <p className="m-0 px-3">Upload</p>
                  </ButtonComponent>
                </Space>
              </Form.Item>
            </div>
          </div>
        </Form>
      </div>
    )
  }
)

export { UpdateCVComponent }
