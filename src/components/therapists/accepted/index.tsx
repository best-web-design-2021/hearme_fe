import { GDoctor } from "@/utils"
import { Avatar, Space, Form, Select, Input, DatePicker, Row, Col } from "antd"
import React, { memo } from "react"
import { doctor2 } from "@assets"
import "./styles.scss"
import styles from "./styles.module.scss"
import ButtonComponent from "@components/button"
import { InfoCircleOutlined, CheckOutlined } from "@ant-design/icons"
import moment from "moment"

const TextArea = Input
interface AcceptedRegisterProps {
  doctorInformation?: GDoctor
}

const AcceptedRegister: React.FC<AcceptedRegisterProps> = memo(
  ({ doctorInformation }) => {
    return (
      <section className={`cv-accepted-wrap ${styles.wrap}`}>
        <div
          className={`p-3 ${styles.titleWrap} d-flex align-items-center justify-content-center`}
        >
          <Space align="center" size="large">
            <InfoCircleOutlined className={styles.icon} />
            <Space direction="vertical">
              <h1 className="m-0">Confirm your infomation</h1>
              <p className="m-0">
                Please double-check your information before clicking the confirm
                button; if there are any errors, please contact us to have them
                corrected. Have a wonderful day!
              </p>
            </Space>
          </Space>
        </div>
        <div className="container pt-4 d-flex flex-column flex-md-row">
          <div
            className={` ${styles.basicWrap} d-flex align-items-center align-items-md-start justify-content-md-start justify-content-center`}
          >
            <Avatar src={doctor2} size={180} />
          </div>
          <Form className="w-100 form-container">
            <Row>
              <Col lg={12} md={12} sm={12} xs={24}>
                <Form.Item label="Name" className="d-inline">
                  <Input
                    size="large"
                    value={doctorInformation.user.lastName}
                    disabled
                    className="w-100"
                  />
                </Form.Item>
              </Col>
              <Col lg={12} md={12} sm={12} xs={24} className="px-sm-2 ">
                <Form.Item label="Experince" className="d-inline">
                  <DatePicker
                    size="large"
                    value={moment(doctorInformation.exprience)}
                    disabled
                  />
                </Form.Item>
              </Col>
            </Row>

            <Form.Item label="Intro" className="d-inline">
              <TextArea
                size="large"
                value={doctorInformation.slogan}
                disabled
                className="w-100"
              ></TextArea>
            </Form.Item>
            <Form.Item label="Therapies Offered" className="d-inline">
              <Select
                size="large"
                mode="tags"
                value={doctorInformation.offeres}
                disabled
              ></Select>
            </Form.Item>
            <Form.Item label="Top Specialties" className="d-inline">
              <Select
                size="large"
                mode="tags"
                value={doctorInformation.specialties}
                disabled
              ></Select>
            </Form.Item>
            <Form.Item className="button-wrap">
              <ButtonComponent
                className={`d-flex align-items-center ${styles.button}`}
              >
                <CheckOutlined /> <p className="text-white m-0">Confirm</p>
              </ButtonComponent>
            </Form.Item>
          </Form>
        </div>
      </section>
    )
  }
)

export { AcceptedRegister }


