import React, { memo } from "react"
import { Progress, Row, Col } from 'antd'
import styles from './styles.module.scss'
import { useAuth } from "@context"
import './styles.scss'


interface IncomeProps { }

const Income: React.FC<IncomeProps> = memo(() => {
    var data = [
        {
          type: 'profit',
          value: 68,
        },
        {
        type: 'rest',
        value: 32
        }
        
      ];
    const { user } = useAuth()
   
    return (
        <section className={`${styles.wrap} income-wrap w-100 bg-white border-radius-12 p-3 shadow-css`}>
            <Row>
                <Col span={12}>
                    <h4>Earnings</h4>
                    <p className='m-0'>Total expense</p>
                    <h3 className='pt-4'>$570.00</h3>
                    <p>Profit is 68% more than last Month</p>
                </Col>
                <Col span={12} className='d-flex align-items-center justify-content-center'> <Progress
                trailColor='#f7fafb'
                width={180}
                strokeColor='#1877f2'
                strokeWidth={13}
                type="circle" percent={68} /></Col>
            </Row>
            
        </section>
    )
})

export { Income }
