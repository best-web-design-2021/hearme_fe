import React, { memo } from "react"
import styles from './styles.module.scss'
import ShowMore from "@components/show-more"
import { Item } from './item'

interface ReportsProps { }

const Reports: React.FC<ReportsProps> = memo(() => {
   
    return (
        <section className={`${styles.wrap} w-100 bg-white border-radius-12 p-4 shadow-css`}>
         <div className='d-flex align-items-center justify-content-between'>
          <h4>Reports</h4>
          <ShowMore menu=''/>
          </div>
          <div className={styles.itemContainer}>
          <Item />
          <Item />
          <Item />
          </div>
        </section>
    )
})

export { Reports }
