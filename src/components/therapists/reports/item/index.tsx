import React, { memo } from "react"
import { Card, Space } from 'antd'
import styles from '../styles.module.scss'
import { SettingOutlined } from "@ant-design/icons"
import './styles.scss'


interface ItemProps { }

const Item: React.FC<ItemProps> = memo(() => {

    return (
        <div className={`report-item-wrap my-2 ${styles.itemWrap}`}>
            <Card>
                <Space align='start' size='middle'>
                    <div>
                        <SettingOutlined />
                    </div>

                    <Space direction='vertical'>
                        <p className={`m-0 text-justify ${styles.content}`}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Impedit, excepturi! Quas, sequi quos eveniet quaerat nostrum odit ut possimus fugiat.</p>
                        <span className={`m-0 ${styles.time}`}>10 minutes ago</span>
                    </Space>
                </Space>

            </Card>
        </div>
    )
})

export { Item }
