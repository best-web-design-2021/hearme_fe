import React, { memo } from "react"
import styles from './styles.module.scss'
import { useAuth } from "@context"
import { Column } from '@ant-design/charts';
import ShowMore from "@components/show-more"


interface PatientActivityProps { }

const PatientActivity: React.FC<PatientActivityProps> = memo(() => {
    const data = [
        {
          type: 1,
          sales: 38,
          month: 'Jan'
        },
        {
          type: 2,
          sales: 52,
          month: 'Feb'
        },
        {
          month: 'Mar',
          sales: 61,
          type: 3
        },
        {
          month: 'Apr',
          sales: 145,
          type: 4
        },
        {
          month: 'May',
          sales: 48,
          type: 5
        },
        {
          month: 'Jun',
          sales: 38,
          type: 6
        },
        // {
        //   month: 'July',
        //   sales: 38,
        //   type: 7
        // },
        // {
        //   month: 'Aug',
        //   sales: 38,
        //   type: 8
        // },
        // {
        //   month: 'Sep',
        //   sales: 38,
        //   type: 9
        // },
        // {
        //     month: 'Oct',
        //     sales: 38,
        //     type: 10
        //   },
        // {
        //   month: 'Nov',
        //   sales: 38,
        //   type: 11
        // },
        // {
        //   month: 'Dec',
        //   sales: 38,
        //   type: 12
        // },
      ];
    const { user } = useAuth()
    const config = {
        appendPadding: 20,
        height: 280,
        title: {
            text: 'texxt'
        },
        maxColumnWidth: 20,
        columnStyle: {
            style: {
                lineWidth: 0.5,
            },
        },
        data: data,
        xField: 'month',
        yField: 'sales',
        meta: {
          type: { alias: '类别' },
          sales: { alias: '销售额' },
        },
        color: ({ month }) => {
            console.log('h', month)
            if(month == 'Jan' || month == 'Mar' || month == 'May' || month == 'July' || month == 'Sep' || month == 'Nov') {
                return '#D9E5FC'
            }
            return '#1877f2'
        }
      };
    return (
        <section className={`${styles.wrap} w-100 bg-white border-radius-12 p-4 shadow-css`}>
         <div className='d-flex align-items-center justify-content-between'>
          <h4>Patient activities</h4>
          <ShowMore menu=''/>
          </div>
          <div>
           <Column {...config} />
           </div>
        </section>
    )
})

export { PatientActivity }
