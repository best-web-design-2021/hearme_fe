import React, { memo } from "react"
import styles from "./styles.module.scss"
import { waiting } from "@assets"

interface PedingRegisterProps {}

const PedingRegister: React.FC<PedingRegisterProps> = memo(() => {
  return (
    <section className={`${styles.wrap} text-center`}>
      <div className="px-lg-5">
        <img src={waiting} />
      </div>
      <h1>Aww.. We are reviewing your CV</h1>
      <p className={`text-uppercase ${styles.textContent}`}>
        We'll be in touch with you as soon as posible.
      </p>
      <p>Have a nice day!</p>
    </section>
  )
})

export { PedingRegister }
