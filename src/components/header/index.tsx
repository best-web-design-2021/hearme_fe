import React, { memo, useCallback, useEffect, useState } from "react"
import { logoNgang } from "@assets"
import { Switch } from 'antd'
import { Link } from "react-router-dom"
import { useTranslation } from 'react-i18next'
import "./styles.scss"
import styles from "./styles.module.scss"
import { EnumLanguages } from "@/translation"
import { LANGUAGE } from "@/constants"
interface HeaderComponentProps {
  visible?: boolean
}

const HeaderComponent: React.FC<HeaderComponentProps> = memo(
  ({  }) => {
    const [header, setHeader] = useState("")
    const { t, i18n } = useTranslation("")

    useEffect(() => {
      window.addEventListener("scroll", listenScrollEvent)

      return () => window.removeEventListener("scroll", listenScrollEvent)
    }, [])

    const listenScrollEvent = event => {
      if (window.scrollY < 73) {
        return setHeader("")
      } else if (window.scrollY > 70) {
        return setHeader("header")
      }
    }

    const changeLanguage = useCallback((check: boolean) => {
      if (check) {
        i18n.changeLanguage(EnumLanguages.VI)
        localStorage.setItem(LANGUAGE, EnumLanguages.VI)
      }
      else {
        i18n.changeLanguage(EnumLanguages.EN)
        localStorage.setItem(LANGUAGE, EnumLanguages.EN)
      }
      }, [i18n])

    return (
      <nav
        className={`navbar navbar-expand-lg navbar-light bg-white header-noauth  ${header}`}
        style={{ position: "absolute", right: 0, left: 0 }}
      >
        <div className={styles.headerContent}>
          <Link to="/">
            <img className="" src={logoNgang} alt="logo" />
          </Link>
        </div>
        <div className={styles.headerSwitchLang}>
          <Switch
          size='default'
            checkedChildren={'Vi'}
            unCheckedChildren={"En"}
            // defaultChecked={}
            checked={i18n.language === EnumLanguages.VI}
            onChange={changeLanguage}
          />
        </div>
      </nav>
    )
  }
)

export { HeaderComponent }
