import React, { CSSProperties, memo, useState } from "react"
import styles from "./styles.module.scss"
import { Button } from "antd"

interface buttonProps {
  onPress?: Function
  type?: "button" | "submit" | "reset"
  style?: CSSProperties
  className?: string
  icon?: any
  disabled?: boolean
  loading?: boolean
  test?: any
}

const button: React.FC<buttonProps> = memo(
  ({ children, type, style, className, onPress, icon, disabled, loading }) => {
    return (
      // <div className={styles.container}>
      <Button
        loading={loading}
        disabled={disabled}
        className={`w-100 d-flex align-items-center justify-content-center text-white border-radius-12 btn ${styles.btn} ${className}`}
        htmlType={type}
        style={style}
        onClick={() => onPress && onPress()}
      >
        {children}
      </Button>
    )
  }
)

export default button
