import { ClockCircleOutlined } from "@ant-design/icons"
import React, { memo } from "react"
import styles from "./styles.module.scss"
import "./styles.scss"
import { useTranslation } from 'react-i18next'

interface GoalItemsProps {
  title?: string
  content?: string
  icon?: any
}

const GoalItem: React.FC<GoalItemsProps> = ({ title, content, icon }) => {
  const { t } = useTranslation()
  return (
    <div className={` container p-4 ${styles.container} goal-item-wrap`}>
      <div className="text-center">
        {icon}
        {/* <ClockCircleOutlined className={styles.icon} /> */}
        <h4 className="pt-3">{title}</h4>
        <p className="pt-3">{content}</p>
      </div>
    </div>
  )
}

export default memo(GoalItem)
