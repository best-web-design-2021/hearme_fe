import React, { memo, useCallback, useEffect, useState } from "react"
import { logoNgang } from "@assets"
import { Link, useHistory, useLocation } from "react-router-dom"
import { Divider, Form, Input, Checkbox, notification, Button } from "antd"
import { LockOutlined, RightOutlined, UserOutlined } from "@ant-design/icons"
import ButtonComponent from "@components/button"
import { useAuth } from "@context"
import { BsSlash } from "react-icons/bs"
import { useForm } from "antd/lib/form/Form"
import { mutateData, queryData } from "@/tools/apollo/func"
import { LOGIN, LOGIN_GOOGLE } from "@/graphql/mutation"
import { ACCESS_TOKEN, PREV_ROUTER } from "@constants"
import { ME } from "@/graphql/query"
import {
  ApolloError,
} from "@apollo/client"
import { useTranslation } from 'react-i18next'
import { GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login'
import FacebookLogin from 'react-facebook-login'
// import TiSocialFacebookCircular from 'react-icons/lib/ti/social-facebook-circular';

import "./styles.scss"
import styles from "./styles.module.scss"
import { useServiceMutation } from "@/utils/hooks/graphql"

const client_id = process.env.GOOGLE_CLIENT_ID

interface LoginFormProps { }

const LoginForm: React.FC<LoginFormProps> = memo(() => {
  const { isAuth, dispatchAuth } = useAuth()
  const history = useHistory()
  const location = useLocation()
  const [form] = useForm()
  const [loading, setLoading] = useState<boolean>(false)
  const [loginGG, { loading: loadingGoogle, data, error }] = useServiceMutation<{ loginGoogle: { token: string } }>(LOGIN_GOOGLE)
  const { t } = useTranslation()

  useEffect(() => {
    if (!data || !data.loginGoogle) return
    dispatchAndSetToken(data?.loginGoogle?.token)
  }, [data])

  useEffect(() => {
    if (!error) return
    notification.error({
      message: "Đăng nhập thất bại"
    })
  }, [error])

  useEffect(() => {
    return () => localStorage.removeItem(PREV_ROUTER)
  }, [])

  useEffect(() => {
    if (isAuth) history.replace("/")
  }, [isAuth, history])

  const onFinish = (values: any) => {
    setLoading(true)
    mutateData(LOGIN, {
      info: values
    })
      .then(async ({ data, errors }) => {
        if (errors || !data?.login?.token) {
          notification.error({
            message: "Đăng nhập thất bại"
          })
          return
        }
        await dispatchAndSetToken(data?.login?.token)
      })
      .catch((error: ApolloError) => {
        error?.graphQLErrors.forEach(err => {
          notification.error({
            message: err?.message
          })
        })
      })
      .finally(() => setLoading(false))
    return
  }

  const dispatchAndSetToken = async (token) => {
    localStorage.setItem(ACCESS_TOKEN, token)
    const me = await fetchMe()
    if (!me) throw new Error()
    dispatchAuth({
      type: "SET_AUTHEN",
      payload: {
        user: me,
        isAuth: true
      }
    })
    notification.success({
      message: "Đăng nhập thành công"
    })
  }

  const fetchMe = async () => {
    try {
      const { data, errors } = await queryData(ME)
      if (errors) return null
      return data.me
    } catch (error) {
      // error?.graphQLErrors
    }
  }

  const responseGoogle = useCallback((response: GoogleLoginResponse) => {
    console.log(response)
    loginGG({
      variables: {
        token: response?.tokenId
      }
    })
  }, [])

  const onFailedGG = useCallback((error: any) => {
    console.log(error)
  }, [])

  const onFinishFailed = (errorInfo: any) => { }
  return (
    <div className={`pt-5 ${styles.container}`}>
      <section className={`d-flex ${styles.logoWrap}`}>
        <Link to="/"><img src={logoNgang} /></Link>
        <p className="m-0 "> - Be nice.</p>
      </section>
      <section className={`pt-4 ${styles.formTitleWrap}`}>
        <div className={styles.divider}>
          <span className="text-uppercase">User Login</span>
        </div>
        <h1 className="pt-2">
          Millions of Quality resources
          <BsSlash />
        </h1>
        <Divider className="mt-5 mb-0" />
      </section>
      <Form
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        form={form}
        name="basic"
        initialValues={{ remember: true }}
      >
        <Form.Item
          className="d-inline"
          label="Username"
          name="username"
          rules={[{ required: true, message: "Please input your username!" }]}
        >
          <Input
            size="large"
            className={`${styles.inputWrap}`}
            prefix={<UserOutlined />}
          />
        </Form.Item>

        <Form.Item
          label="Password"
          className="d-inline"
          name="password"
          rules={[{ required: true, message: "Please input your password!" }]}
        >
          <Input.Password
            size="large"
            className={`${styles.inputWrap}`}
            prefix={<LockOutlined />}
          />
        </Form.Item>

        <div className="py-3">
          <div className="d-flex align-content-center justify-content-between">
            <Checkbox>Remember me</Checkbox>
            <span>Forget Password</span>
          </div>
        </div>

        <Form.Item>
          <ButtonComponent
            className={`d-flex align-items-center justify-content-center ${styles.button}`}
            type="submit"
            loading={loading}
            disabled={loading || loadingGoogle}
          >
            <p className="m-0">{t("pages.sign-in.sign-in")}</p>
          </ButtonComponent>
        </Form.Item>
        <Form.Item>
          <GoogleLogin
            clientId={client_id}
            buttonText={t("pages.sign-in.sign-in-with-google")}
            className={`d-flex align-items-center justify-content-center ${styles.button}`}
            onSuccess={responseGoogle}
            onFailure={onFailedGG}
            cookiePolicy={'single_host_origin'}
            disabled={loading || loadingGoogle}
            autoLoad={loadingGoogle}
          />
        </Form.Item>
        {/* <Form.Item>
          <FacebookLogin
            appId="1088597931155576"
            autoLoad={true}
            fields="name,email,picture"
            cssClass={`d-flex  align-items-center justify-content-center ${styles.button}`}
            // icon={TiSocialFacebookCircular}
          // onClick={componentClicked}
          callback={responseFacebook} 
          />
        </Form.Item> */}

      </Form>
      <div
        className={` ${styles.registerWrap} pt-3  d-inline-flex align-items-center`}
      >
        <p className={`${styles.noAcc} m-0`}>No account yet? </p>
        <Link
          to=""
          className={`${styles.register} d-inline-flex align-items-center`}
        >
          <p className="m-0">Registerd</p>
          <RightOutlined className={styles.rightIcon} />
        </Link>
      </div>
    </div>
  )
})

export { LoginForm }
