
import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'
import { LANGUAGE } from '@/constants'

const vietnamese = require('./languages/vi.json')
const english = require('./languages/en.json')

export enum EnumLanguages {
    VI = 'vi',
    EN = 'en'
}

const languageDetector: any = {
    type: 'languageDetector',
    async: true,
    detect: (cb) => cb(localStorage.getItem(LANGUAGE) || 'en'),
    init: () => { },
    cacheUserLanguage: () => { }
}

i18n
    .use(languageDetector)
    .use(initReactI18next)
    .init({
        resources: {
            en: {
                translation: english
            },
            vi: {
                translation: vietnamese
            }
        },
        fallbackLng: ['vi', 'en'],
        interpolation: {
            escapeValue: false,
            formatSeparator: ','
        },
        react: {
            wait: true
        },
        returnObjects: true
    })

export {
    i18n
}
